/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <google/protobuf/compiler/plugin.h>

#include <QCommandLineParser>
#include <QString>

#include "lvd/application.hpp"
#include "lvd/logger.hpp"
#include "lvd/metatype.hpp"

#include "namespaces.hpp"         // IWYU pragma: keep
#include "protobuf_qpp_impl.hpp"  // IWYU pragma: keep
#include "protobuf_qpp_main.hpp"  // IWYU pragma: keep

#include "config.hpp"

// ----------

int main(int argc, char* argv[]) {
  lvd::Metatype::metatype();

  lvd::Application::setApplicationName (lvd::pqpp::config::App_Name());
  lvd::Application::setApplicationVersion(lvd::pqpp::config::App_Vers());

  lvd::Application::setOrganizationName(lvd::pqpp::config::Org_Name());
  lvd::Application::setOrganizationDomain(lvd::pqpp::config::Org_Addr());

  lvd::Application app(argc, argv);

  lvd::Application::connect(&app, &lvd::Application::failure,
    [] (const QString& message) {
      if (!message.isEmpty()) {
        lvd::qStdErr() << message << Qt::endl;
      }

      lvd::Application::exit(1);
    });

  // ----------

  lvd::Logger::install();
  LVD_LOGFUN

  // ----------

  QCommandLineParser qcommandlineparser;
  qcommandlineparser.   addHelpOption();
  qcommandlineparser.addVersionOption();

  lvd::Logger::setup_arguments(qcommandlineparser);

  qcommandlineparser.process(app);

  lvd::Logger::parse_arguments(qcommandlineparser);
  lvd::Application::print();

  // ----------

  lvd::pqpp::ProtobufQppMain          protobuf;
  return gpc::PluginMain(argc, argv, &protobuf);
}
