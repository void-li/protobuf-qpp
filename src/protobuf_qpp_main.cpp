/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "protobuf_qpp_main.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QString>

#include "lvd/shield.hpp"

#include "protobuf_qpp_impl.hpp"

// ----------

namespace lvd::pqpp {

bool ProtobufQppMain::Generate(const gp ::FileDescriptor*   file,
                               const std::string&           parameter,
                                     gpc::GeneratorContext* generator_context,
                                     std::string*           error) const {
  return
  LVD_SHIELD;

  ProtobufQppImpl protobufQppImpl(file, generator_context,
                                  QString::fromStdString(parameter));

  protobufQppImpl.protobufQppImpl();

  return true;

  LVD_SHIELD_FUN([&] (const QString& message) {
    *error = message.toStdString();
    return false;
  });
}

}  // namespace lvd::pqpp
