/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "protobuf_qpp_impl.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <algorithm>
#include <cstring>
#include <iterator>
#include <map>
#include <string>

#include <QChar>
#include <QDebug>
#include <QDebugStateSaver>
#include <QFile>
#include <QFileInfo>
#include <QIODevice>
#include <QKeyValueIterator>
#include <QList>
#include <QSaveFile>
#include <QString>
#include <QStringList>
#include <QStringRef>

#include "lvd/string.hpp"

// ----------

QDebug operator<<(QDebug qdebug, const std::string& s) {
  QDebugStateSaver saver(qdebug);

  qdebug << s.data();
  return qdebug;
}

// ----------

namespace {

QString make_lower_camel_name(const QStringList& names) {
  QString lower_camel_name;

  for (const QString& name : names) {
    if (!name.isEmpty()) {
      if (lower_camel_name.isEmpty()) {
        lower_camel_name.append(name.at(0).toLower() + name.mid(1));
      } else {
        lower_camel_name.append(name.at(0).toUpper() + name.mid(1));
      }
    }
  }

  return lower_camel_name;
}
QString make_lower_camel_name(const std::string& name ) {
  return make_lower_camel_name(QStringList(QString::fromStdString(name)));
}

QString make_upper_camel_name(const QStringList& names) {
  QString upper_camel_name;

  for (const QString& name : names) {
    if (!name.isEmpty()) {
      if (upper_camel_name.isEmpty()) {
        upper_camel_name.append(name.at(0).toUpper() + name.mid(1));
      } else {
        upper_camel_name.append(name.at(0).toUpper() + name.mid(1));
      }
    }
  }

  return upper_camel_name;
}
QString make_upper_camel_name(const std::string& name ) {
  return make_upper_camel_name(QStringList(QString::fromStdString(name)));
}

QString make_lower_snake_name(const QStringList& names) {
  QString lower_snake_name;

  for (const QString& name : names) {
    if (!name.isEmpty()) {
      if (lower_snake_name.isEmpty()) {
        lower_snake_name.append(name.at(0).toLower() + name.mid(1));
      } else {
        lower_snake_name.append('_');
        lower_snake_name.append(name.at(0).toLower() + name.mid(1));
      }
    }
  }

  return lower_snake_name;
}
QString make_lower_snake_name(const std::string& name ) {
  return make_lower_snake_name(QStringList(QString::fromStdString(name)));
}

QString make_upper_snake_name(const QStringList& names) {
  QString upper_snake_name;

  for (const QString& name : names) {
    if (!name.isEmpty()) {
      if (upper_snake_name.isEmpty()) {
        upper_snake_name.append(name.at(0).toUpper() + name.mid(1));
      } else {
        upper_snake_name.append('_');
        upper_snake_name.append(name.at(0).toUpper() + name.mid(1));
      }
    }
  }

  return upper_snake_name;
}
QString make_upper_snake_name(const std::string& name ) {
  return make_upper_snake_name(QStringList(QString::fromStdString(name)));
}

QString make_minor_snake_name(const QStringList& names) {
  QString minor_snake_name;

  for (const QString& name : names) {
    if (!name.isEmpty()) {
      if (minor_snake_name.isEmpty()) {
        minor_snake_name.append(name.at(0).toLower() + name.mid(1).toLower());
      } else {
        minor_snake_name.append('_');
        minor_snake_name.append(name.at(0).toLower() + name.mid(1).toLower());
      }
    }
  }

  return minor_snake_name;
}
QString make_minor_snake_name(const std::string& name ) {
  return make_minor_snake_name(QStringList(QString::fromStdString(name)));
}

// ----------

QString make_pure_name(const gp::     Descriptor* descriptor) {
  QString     pure_name = QString::fromStdString(descriptor->name());
  QStringList pure_name_list(pure_name);

  std::reverse(pure_name_list.begin(), pure_name_list.end());
  pure_name = make_upper_snake_name(pure_name_list);

  return pure_name;
}

QString make_pure_name(const gp:: EnumDescriptor* descriptor) {
  QString     pure_name = QString::fromStdString(descriptor->name());
  QStringList pure_name_list(pure_name);

  std::reverse(pure_name_list.begin(), pure_name_list.end());
  pure_name = make_upper_snake_name(pure_name_list);

  return pure_name;
}

// ----------

QString make_full_name(const gp::     Descriptor* descriptor) {
  QString     full_name = QString::fromStdString(descriptor->name());
  QStringList full_name_list(full_name);

  auto * descparent = descriptor->containing_type();
  while (descparent) {
    QString name = QString::fromStdString(descparent->name());
    full_name_list.append(name);

    descparent = descparent->containing_type();
  }

  std::reverse(full_name_list.begin(), full_name_list.end());
  full_name = make_upper_snake_name(full_name_list);

  return full_name;
}

QString make_full_name(const gp:: EnumDescriptor* descriptor) {
  QString     full_name = QString::fromStdString(descriptor->name());
  QStringList full_name_list(full_name);

  auto * descparent = descriptor->containing_type();
  while (descparent) {
    QString name = QString::fromStdString(descparent->name());
    full_name_list.append(name);

    descparent = descparent->containing_type();
  }

  std::reverse(full_name_list.begin(), full_name_list.end());
  full_name = make_upper_snake_name(full_name_list);

  return full_name;
}

// ----------

QString make_full_type(const gp::FieldDescriptor* descriptor, bool as_list = false) {
  LVD_LOGFUN_LIKE(lvd::pqpp::ProtobufQppImpl)

  QString type_name;
  QString list_name;

  switch (descriptor->type()) {
    case gp::FieldDescriptor::TYPE_INT32:
      type_name = "qint32";
      break;

    case gp::FieldDescriptor::TYPE_INT64:
      type_name = "qint64";
      break;

    case gp::FieldDescriptor::TYPE_UINT32:
      type_name = "quint32";
      break;

    case gp::FieldDescriptor::TYPE_UINT64:
      type_name = "quint64";
      break;

    case gp::FieldDescriptor::TYPE_SINT32:
      type_name = "qint32";
      break;

    case gp::FieldDescriptor::TYPE_SINT64:
      type_name = "qint64";
      break;

    case gp::FieldDescriptor::TYPE_BOOL:
      type_name = "bool";
      break;

    case gp::FieldDescriptor::TYPE_FIXED32:
      type_name = "quint32";
      break;

    case gp::FieldDescriptor::TYPE_FIXED64:
      type_name = "quint64";
      break;

    case gp::FieldDescriptor::TYPE_FLOAT:
      type_name = "float";
      break;

    case gp::FieldDescriptor::TYPE_SFIXED32:
      type_name = "qint32";
      break;

    case gp::FieldDescriptor::TYPE_SFIXED64:
      type_name = "qint64";
      break;

    case gp::FieldDescriptor::TYPE_DOUBLE:
      type_name = "double";
      break;

    case gp::FieldDescriptor::TYPE_STRING:
      type_name = "QString";
      list_name = "QStringList";
      break;

    case gp::FieldDescriptor::TYPE_BYTES:
      type_name = "QByteArray";
      list_name = "QByteArrayList";
      break;

    case gp::FieldDescriptor::TYPE_MESSAGE:
      type_name = make_full_name(descriptor->message_type());
     break;

    case gp::FieldDescriptor::TYPE_ENUM:
      type_name = make_full_name(descriptor->   enum_type());

      if (descriptor->enum_type()->containing_type()) {
        type_name.clear();

        type_name += make_full_name(descriptor->enum_type()->containing_type());
        type_name += "::";
        type_name += make_pure_name(descriptor->enum_type());
      }
    break;

    default:
      LVD_LOGBUF message;
      LVD_LOG_W(&message) << "unknown gp::FieldDescriptor type"
                          << descriptor->type();

      LVD_THROW_RUNTIME(message);
  }

  if (as_list) {
    if (list_name.isEmpty()) {
      list_name = QString("QList<%1>").arg(type_name);
    }

    return list_name;
  }

  return type_name;
}

}  // namespace

// ----------

namespace lvd::pqpp {

ProtobufQppImpl::ProtobufQppImpl(const gp ::FileDescriptor*   descriptor,
                                       gpc::GeneratorContext* gencontext,
                                 const QString&               cppoutpath)
    : descriptor_(descriptor),
      gencontext_(gencontext),
      cppoutpath_(cppoutpath) {
  LVD_LOG_T();

  // header
  header_name_    = QString::fromStdString(descriptor->name());
  header_name_    = QFileInfo(header_name_).baseName() + ".pb.hpp";
  header_name_pb_ = QFileInfo(header_name_).baseName() + ".pb.h";

  header_stream_.reset(gencontext->Open(header_name_.toStdString()));
  header_writer_ = std::make_unique<gpi::Printer>(header_stream_.get(), '$');

  // source
  source_name_    = QString::fromStdString(descriptor->name());
  source_name_    = QFileInfo(source_name_).baseName() + ".pb.cpp";
  source_name_pb_ = QFileInfo(source_name_).baseName() + ".pb.cc";

  source_stream_.reset(gencontext->Open(source_name_.toStdString()));
  source_writer_ = std::make_unique<gpi::Printer>(source_stream_.get(), '$');
}

ProtobufQppImpl::~ProtobufQppImpl() {
  LVD_LOG_T();
}

// ----------

void ProtobufQppImpl::protobufQppImpl() {
  LVD_LOG_T();

  make_header();
  make_source();
  make_inject();
}

// ----------

void ProtobufQppImpl::make_header() {
  LVD_LOG_T();

  make_header_line("#pragma once");
  make_header_line();

  make_header_line("// IWYU pragma: no_include \"" + header_name_pb_ + "\"");
  make_header_line();

  make_header_includes();

  make_header_line("// ----------");
  make_header_space();

  make_namespace_setup();
  make_header_line();

  make_header_declares();
  make_header_enumeras();
  make_header_messages();

  make_header_line();
  make_namespace_close();
}

// ----------

void ProtobufQppImpl::make_header_includes() {
  LVD_LOG_T();

  for (int i = 0; i < descriptor_->message_type_count(); i ++) {
    const auto item = descriptor_->message_type(i);
    make_header_includes(item);
  }

  if (!header_includes_.isEmpty()) {
    QStringList header_includes = header_includes_.values();
    header_includes.sort();

    for (const QString& header_include : header_includes) {
      make_header_line("#include <$header_include$>",
                       "header_include", header_include);
    }

    make_header_line();
  }
}

void ProtobufQppImpl::make_header_includes(const gp::Descriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  header_includes_.insert("QByteArray");
  header_includes_.insert("QSharedDataPointer");
  source_includes_.insert("QSharedData");
  source_includes_.insert("QtGlobal");

  for (int i = 0; i < descriptor->nested_type_count(); i ++) {
    const auto item = descriptor->nested_type(i);
    make_header_includes(item);
  }

  for (int i = 0; i < descriptor->      field_count(); i ++) {
    const auto item = descriptor->      field(i);

    if (   item->type() == gp::FieldDescriptor::TYPE_INT32
        || item->type() == gp::FieldDescriptor::TYPE_INT64
        || item->type() == gp::FieldDescriptor::TYPE_UINT32
        || item->type() == gp::FieldDescriptor::TYPE_UINT64
        || item->type() == gp::FieldDescriptor::TYPE_SINT32
        || item->type() == gp::FieldDescriptor::TYPE_SINT64
        || item->type() == gp::FieldDescriptor::TYPE_FIXED32
        || item->type() == gp::FieldDescriptor::TYPE_FIXED64
        || item->type() == gp::FieldDescriptor::TYPE_SFIXED32
        || item->type() == gp::FieldDescriptor::TYPE_SFIXED64) {
      header_includes_.insert("QtGlobal");

      if (item->is_repeated()) {
        header_includes_.insert("QList");
      }
    }
    else if (item->type() == gp::FieldDescriptor::TYPE_STRING) {
      header_includes_.insert("QString");

      if (item->is_repeated()) {
        header_includes_.insert("QStringList");
      }
    }
    else if (item->type() == gp::FieldDescriptor::TYPE_BYTES ) {
      header_includes_.insert("QByteArray");

      if (item->is_repeated()) {
        header_includes_.insert("QByteArrayList");
      }
    }
    else {
      if (item->is_repeated()) {
        header_includes_.insert("QList");
      }
    }
  }
}

// ----------

void ProtobufQppImpl::make_header_declares() {
  LVD_LOG_T();

  if (descriptor_->message_type_count() > 0) {
    make_header_split_if_necessary();
    require_header_split();

    make_header_line("namespace __pqpp__ {");

    for (int i = 0; i < descriptor_->message_type_count(); i ++) {
      const auto item = descriptor_->message_type(i);
      make_header_declare(item);
    }

    make_header_line("}  // namespace __pqpp__");
  }
}

void ProtobufQppImpl::make_header_declares(const gp::Descriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  for (int i = 0; i < descriptor -> nested_type_count(); i ++) {
    const auto item = descriptor -> nested_type(i);
    make_header_declare(item);
  }
}

void ProtobufQppImpl::make_header_declare (const gp::Descriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  HeaderVars header_vars;
  header_vars["class_name"] = make_full_name(descriptor);

  if (descriptor->nested_type_count() > 0) {
    make_header_declares(descriptor);
  }

  make_header_line("class $class_name$;",
                   header_vars);
}

// ----------

void ProtobufQppImpl::make_header_enumeras() {
  LVD_LOG_T();

  for (int i = 0; i < descriptor_->   enum_type_count(); i ++) {
    const auto item = descriptor_->   enum_type(i);
    make_header_enumera(item);
  }
}

void ProtobufQppImpl::make_header_enumeras(const gp::    Descriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  for (int i = 0; i < descriptor ->   enum_type_count(); i ++) {
    const auto item = descriptor ->   enum_type(i);
    make_header_enumera(item);
  }
}

void ProtobufQppImpl::make_header_enumera (const gp::EnumDescriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  HeaderVars header_vars;
  header_vars["enume_name"] = make_pure_name(descriptor);

  if (!descriptor->containing_type()) {
    make_header_split_if_necessary();
    require_header_split();
  }

  incr_header_line("enum $enume_name$ {",
                   header_vars);

  for (int i = 0; i < descriptor->value_count(); i ++) {
    const auto item = descriptor->value(i);

    header_vars["enume_item"] = QString::fromStdString(item->name  ());
    header_vars["enume_eval"] = QString::       number(item->number());

    make_header_line("$enume_item$ = $enume_eval$,",
                     header_vars);
  }

  decr_header_line("};");

  make_header_line();

  if (!descriptor->containing_type()) {
    make_header_line(       "bool $enume_name$_IsValid(int value);",
                     header_vars);
  } else {
    make_header_line("static bool $enume_name$_IsValid(int value);",
                     header_vars);
  }
}

// ----------

void ProtobufQppImpl::make_header_messages() {
  LVD_LOG_T();

  for (int i = 0; i < descriptor_->message_type_count(); i ++) {
    const auto item = descriptor_->message_type(i);
    make_header_message(item);
  }
}

void ProtobufQppImpl::make_header_messages(const gp::Descriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  for (int i = 0; i < descriptor -> nested_type_count(); i ++) {
    const auto item = descriptor -> nested_type(i);
    make_header_message(item);
  }
}

void ProtobufQppImpl::make_header_message (const gp::Descriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  HeaderVars header_vars;
  header_vars["class_name"] = make_full_name(descriptor);

  if (descriptor->nested_type_count() > 0) {
    make_header_messages(descriptor);
  }

  make_header_split_if_necessary();
  require_header_split();

  incr_header_line("class $class_name$ {",
                   header_vars);

  header_space_necessary_ = false;

  if (descriptor->  enum_type_count() > 0) {
    make_header_space_if_necessary();
    require_header_space();

    make_header_message_scope("public");

    for (int i = 0; i < descriptor->  enum_type_count(); i ++) {
      const auto item = descriptor->  enum_type(i);

      make_header_enumera(item);

      require_header_space();
    }
  }

  if (descriptor->nested_type_count() > 0) {
    make_header_space_if_necessary();
    require_header_space();

    make_header_message_scope("public");

    for (int i = 0; i < descriptor->nested_type_count(); i ++) {
      const auto item = descriptor->nested_type(i);

      header_vars["neste_name"] = make_pure_name(item);
      header_vars["neste_path"] = make_full_name(item);

      make_header_line("using $neste_name$ = $neste_path$;",
                       header_vars);

      require_header_space();
    }
  }

  make_header_space_if_necessary();
  require_header_space();

  make_header_message_scope("public");

  make_header_line("$class_name$();",
                   header_vars);

  header_vars["space"] = QString(" ").repeated(header_vars["class_name"].size());

  make_header_line("$class_name$(const QByteArray$space$& value,                   bool* is_ok = nullptr);",
                   header_vars);

  make_header_line("$class_name$(const QByteArray$space$& value, int pos, int len, bool* is_ok = nullptr);",
                   header_vars);

  make_header_line("$class_name$(const __pqpp__::$class_name$& value);",
                   header_vars);

  make_header_line();
  // ----------

  make_header_line("$class_name$(const $class_name$&  value);",
                   header_vars);

  make_header_line("$class_name$(      $class_name$&& value);",
                   header_vars);

  make_header_line("~$class_name$();",
                   header_vars);

  make_header_line();
  // ----------

  make_header_line("$class_name$& operator= (const $class_name$&  value);",
                   header_vars);

  make_header_line("$class_name$& operator= (      $class_name$&& value);",
                   header_vars);

  make_header_line();
  // ----------

  make_header_line("QByteArray serialize() const;");

  make_header_line();
  // ----------

  make_header_line("const __pqpp__::$class_name$& __pqpp__() const;",
                   header_vars);

  make_header_line();
  // ----------

  if (descriptor->field_count() > 0) {
    make_header_message_scope("public");
    make_header_message_fields(descriptor);
  }

  make_header_space_if_necessary();
  require_header_space();

  make_header_message_scope("private");

  make_header_line("class              __Data__;");
  make_header_line("QSharedDataPointer<__Data__> d_;");

  decr_header_line("};");
}

// ----------

void ProtobufQppImpl::make_header_message_scope(const QString& scope) {
  LVD_LOG_T() << scope;

  decr_header_line();

  make_header_line(" $scope$:",
                   "scope", scope);

  incr_header_line();

  header_space_necessary_ = false;
}

// ----------

void ProtobufQppImpl::make_header_message_fields(const gp::     Descriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  for (int i = 0; i < descriptor->field_count(); i ++) {
    const auto item = descriptor->field(i);
    make_header_message_field(item);
  }
}

void ProtobufQppImpl::make_header_message_field (const gp::FieldDescriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  HeaderVars header_vars;
  header_vars["class_name"     ] = make_full_name(descriptor->containing_type());

  header_vars["field_type"     ] = make_full_type(descriptor);
  header_vars["field_type_list"] = make_full_type(descriptor, true);

  header_vars["field_rype"     ] = make_full_type(descriptor);
  header_vars["field_rype_list"] = make_full_type(descriptor, true);

  if (header_vars["field_type"].startsWith(header_vars["class_name"])) {
    header_vars["field_type"].replace(
        header_vars["field_type"].indexOf(header_vars["class_name"]),
        header_vars["class_name"].size(), "");

    while (   header_vars["field_type"].startsWith('_')
           || header_vars["field_type"].startsWith(':')) {
      header_vars["field_type"] = header_vars["field_type"].mid(1);
    }
  }

  if (header_vars["field_rype"].startsWith(header_vars["class_name"])) {
    header_vars["field_rype"].replace(
        header_vars["field_rype"].indexOf(header_vars["class_name"]),
        header_vars["class_name"].size(), "");

    while (   header_vars["field_rype"].startsWith('_')
           || header_vars["field_rype"].startsWith(':')) {
      header_vars["field_rype"] = header_vars["field_rype"].mid(1);
    }
  }

  header_vars["field_name_lower_camel"] = make_lower_camel_name(descriptor->name());
  header_vars["field_name_upper_camel"] = make_upper_camel_name(descriptor->name());

  header_vars["field_name_lower_snake"] = make_lower_snake_name(descriptor->name());
  header_vars["field_name_upper_snake"] = make_upper_snake_name(descriptor->name());

  header_vars["field_name_minor_snake"] = make_minor_snake_name(descriptor->name());

  make_header_space_if_necessary();
  require_header_space();

  if (!descriptor->is_repeated()) {
    make_header_line("$field_rype$ $field_name_lower_camel$() const;",
                     header_vars);

    make_header_line("bool has$field_name_upper_camel$() const;",
                     header_vars);

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES
          || descriptor->type() == gp::FieldDescriptor::TYPE_STRING
          || descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      make_header_line("void set$field_name_upper_camel$(const $field_type$& value);",
                       header_vars);
    } else {
      make_header_line("void set$field_name_upper_camel$("    "$field_type$ value);",
                       header_vars);
    }

    make_header_line("void clear$field_name_upper_camel$();",
                     header_vars);
  }
  else {
    make_header_line("$field_rype_list$ $field_name_lower_camel$()          const;",
                     header_vars);

    header_vars["space_a"] = QString(" ").repeated(header_vars["field_type_list"].indexOf(header_vars["field_type"]));
    header_vars["space_b"] = QString(" ").repeated(header_vars["field_type_list"].size() -header_vars["field_type_list"].indexOf(header_vars["field_type"]) -header_vars["field_type"].size());

    make_header_line("$space_a$$field_rype$ $space_b$$field_name_lower_camel$(int index) const;",
                     header_vars);

    make_header_line();

    make_header_line("bool has$field_name_upper_camel$() const;",
                     header_vars);

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES
          || descriptor->type() == gp::FieldDescriptor::TYPE_STRING
          || descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      make_header_line("void set$field_name_upper_camel$(           const $field_type_list$& value);",
                      header_vars);
    }
    else {
      make_header_line("void set$field_name_upper_camel$(const $field_type_list$& value);",
                      header_vars);
    }

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES
          || descriptor->type() == gp::FieldDescriptor::TYPE_STRING
          || descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      header_vars["space_a"] = QString(" ").repeated(header_vars["field_type_list"].indexOf(header_vars["field_type"]));
      header_vars["space_b"] = QString(" ").repeated(header_vars["field_type_list"].size() -header_vars["field_type"].size() -header_vars["space_a"].size());

      make_header_line("void set$field_name_upper_camel$(int index, const $space_a$$field_type$$space_b$& value);",
                       header_vars);
    }
    else {
      header_vars["space_a"] = QString(" ").repeated(header_vars["field_type_list"].indexOf(header_vars["field_type"]));
      header_vars["space_b"] = QString(" ").repeated(header_vars["field_type_list"].size() -header_vars["field_type"].size() -header_vars["space_a"].size());

      header_vars["space_a"] = QString(" ").repeated(header_vars["field_type_list"].indexOf(header_vars["field_type"])
          -static_cast<int>(strlen("int index, ")) +static_cast<int>(strlen("const ")));

      make_header_line("void set$field_name_upper_camel$(int index, "    "$space_a$$field_type$$space_b$  value);",
                       header_vars);
    }

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES
          || descriptor->type() == gp::FieldDescriptor::TYPE_STRING
          || descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      header_vars["space_a"] = QString(" ").repeated(header_vars["field_type_list"].indexOf(header_vars["field_type"]));
      header_vars["space_b"] = QString(" ").repeated(header_vars["field_type_list"].size() -header_vars["field_type"].size() -header_vars["space_a"].size());

      make_header_line("void add$field_name_upper_camel$(           const $space_a$$field_type$$space_b$& value);",
                       header_vars);
    }
    else {
      header_vars["space_a"] = QString(" ").repeated(header_vars["field_type_list"].indexOf(header_vars["field_type"]));
      header_vars["space_b"] = QString(" ").repeated(header_vars["field_type_list"].size() -header_vars["field_type"].size() -header_vars["space_a"].size());

      header_vars["space_a"] = QString(" ").repeated(header_vars["field_type_list"].indexOf(header_vars["field_type"])
          -static_cast<int>(strlen("int index, ")) +static_cast<int>(strlen("const ")));

      make_header_line("void add$field_name_upper_camel$(           "    "$space_a$$field_type$$space_b$  value);",
                       header_vars);
    }

    make_header_line();

    make_header_line("void clear$field_name_upper_camel$();",
                     header_vars);

    make_header_line("int  $field_name_lower_camel$Size() const;",
                     header_vars);
  }
}

// ----------

void ProtobufQppImpl::make_source() {
  LVD_LOG_T();

  make_source_includes();

  make_source_line("#include \"$header_include$\"",
                   "header_include", header_name_);

  make_source_line("#include \"$header_include$\"",
                   "header_include", header_name_pb_);

  make_source_line();

  make_source_line("// ----------");
  make_source_space();

  make_namespace_setup(true);
  make_source_line();

  make_source_enumeras();
  make_source_messages();

  make_source_line();
  make_namespace_close(true);
}

// ----------

void ProtobufQppImpl::make_source_includes() {
  LVD_LOG_T();

  if (!source_includes_.isEmpty()) {
    QStringList source_includes = source_includes_.values();
    source_includes.sort();

    for (const QString& source_include : source_includes) {
      make_source_line("#include <$source_include$>",
                       "source_include", source_include);
    }

    make_source_line();
  }
}

// ----------

void ProtobufQppImpl::make_source_enumeras() {
  LVD_LOG_T();

  for (int i = 0; i < descriptor_->   enum_type_count(); i ++) {
    const auto item = descriptor_->   enum_type(i);
    make_source_enumera(item);
  }
}

void ProtobufQppImpl::make_source_enumeras(const gp::    Descriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  for (int i = 0; i < descriptor ->   enum_type_count(); i ++) {
    const auto item = descriptor ->   enum_type(i);
    make_source_enumera(item);
  }
}

void ProtobufQppImpl::make_source_enumera (const gp::EnumDescriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  SourceVars source_vars;
  source_vars["enume_name"] = make_pure_name(descriptor);

  make_source_split_if_necessary();
  require_source_split();

  if (!descriptor->containing_type()) {
    incr_source_line("bool $enume_name$_IsValid(int value) {",
                     source_vars);

    make_source_line("return __pqpp__::$enume_name$_IsValid(value);",
                     source_vars);

    decr_source_line("}");
  } else {
    source_vars["class_name"] = make_full_name(descriptor->containing_type());

    incr_source_line("bool $class_name$::$enume_name$_IsValid(int value) {",
                     source_vars);

    make_source_line("return __pqpp__::$class_name$::$enume_name$_IsValid(value);",
                     source_vars);

    decr_source_line("}");
  }
}

// ----------

void ProtobufQppImpl::make_source_messages() {
  LVD_LOG_T();

  for (int i = 0; i < descriptor_->message_type_count(); i ++) {
    const auto item = descriptor_->message_type(i);
    make_source_message(item);
  }
}

void ProtobufQppImpl::make_source_messages(const gp::    Descriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  for (int i = 0; i < descriptor -> nested_type_count(); i ++) {
    const auto item = descriptor -> nested_type(i);
    make_source_message(item);
  }
}

void ProtobufQppImpl::make_source_message (const gp::    Descriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  SourceVars source_vars;
  source_vars["class_name"] = make_full_name(descriptor);

  if (descriptor->  enum_type_count() > 0) {
    make_source_enumeras(descriptor);
  }

  if (descriptor->nested_type_count() > 0) {
    make_source_messages(descriptor);
  }

  make_source_split_if_necessary();
  require_source_split();

  incr_source_line("class $class_name$::__Data__ : public QSharedData {",
                   source_vars);

  make_source_message_scope("public");

  make_source_line("__pqpp__::$class_name$ d_;",
                   source_vars);

  decr_source_line("};");
  // ----------

  make_source_split_if_necessary();
  require_source_split();

  make_source_line("$class_name$::$class_name$()",
                   source_vars);
  make_source_line("    : d_(new __Data__()) {}");

  make_source_line();
  // ----------

  source_vars["space"] = QString(" ").repeated(source_vars["class_name"].size());

  make_source_line("$class_name$::$class_name$(const QByteArray$space$& value,                   bool* is_ok)",
                   source_vars);
  incr_source_line("    : d_(new __Data__()) {");

  make_source_line("bool ok = d_->d_.ParseFromArray(value.data(), value.size());");
  make_source_line();

  incr_source_line("if (is_ok) {");
  make_source_line("*is_ok = ok;");
  decr_source_line("}");

  decr_source_line("}");
  make_source_line();
  // ----------

  make_source_line("$class_name$::$class_name$(const QByteArray$space$& value, int pos, int len, bool* is_ok)",
                   source_vars);
  incr_source_line("    : d_(new __Data__()) {");

  make_source_line("Q_ASSERT(pos + len <= value.size());");
  make_source_line();

  make_source_line("bool ok = d_->d_.ParseFromArray(value.data() + pos, len);");
  make_source_line();

  incr_source_line("if (is_ok) {");
  make_source_line("*is_ok = ok;");
  decr_source_line("}");

  decr_source_line("}");
  make_source_line();
  // ----------

  make_source_line("$class_name$::$class_name$(const __pqpp__::$class_name$& value)",
                   source_vars);
  incr_source_line("    : d_(new __Data__()) {");

  make_source_line("d_->d_ = value;");

  decr_source_line("}");
  make_source_line();
  // ----------

  make_source_line("$class_name$::$class_name$(const $class_name$&  value) = default;",
                   source_vars);

  make_source_line("$class_name$::$class_name$(      $class_name$&& value) = default;",
                   source_vars);

  source_vars["space"] = QString(" ").repeated(6 + source_vars["class_name"].size() + 7);

  make_source_line("$class_name$::~$class_name$()$space$ = default;",
                   source_vars);

  make_source_line();
  // ----------

  make_source_line("$class_name$& $class_name$::operator= (const $class_name$&  value) = default;",
                   source_vars);

  make_source_line("$class_name$& $class_name$::operator= (      $class_name$&& value) = default;",
                   source_vars);

  make_source_line();
  // ----------

  make_source_line("QByteArray");
  incr_source_line("$class_name$::serialize() const {",
                   source_vars);

  make_source_line("int value_size = d_->d_.ByteSizeLong();");
  make_source_line();

  make_source_line("QByteArray value;");
  make_source_line("value.resize(value_size);");
  make_source_line();

  make_source_line("d_->d_.SerializeToArray(value.data(), value.size());");
  make_source_line("return value;");

  decr_source_line("}");
  make_source_line();
  // ----------

  incr_source_line("const __pqpp__::$class_name$& $class_name$::__pqpp__() const {",
                   source_vars);

  make_source_line("return d_->d_;");

  decr_source_line("}");
  // ----------

  if (descriptor->field_count() > 0) {
    make_source_message_fields(descriptor);
  }
}

// ----------

void ProtobufQppImpl::make_source_message_scope(const QString& scope) {
  LVD_LOG_T() << scope;

  decr_source_line();

  make_source_line(" $scope$:",
                   "scope", scope);

  incr_source_line();

//  source_space_necessary_ = false;
}

// ----------

void ProtobufQppImpl::make_source_message_fields(const gp::     Descriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  for (int i = 0; i < descriptor->field_count(); i ++) {
    const auto item = descriptor->field(i);
    make_source_message_field(item);
  }
}

void ProtobufQppImpl::make_source_message_field (const gp::FieldDescriptor* descriptor) {
  LVD_LOG_T() << descriptor->name();

  SourceVars source_vars;
  source_vars["class_name"     ] = make_full_name(descriptor->containing_type());

  source_vars["field_type"     ] = make_full_type(descriptor);
  source_vars["field_type_list"] = make_full_type(descriptor, true);

  source_vars["field_rype"     ] = make_full_type(descriptor);
  source_vars["field_rype_list"] = make_full_type(descriptor, true);

  if (source_vars["field_type"].startsWith(source_vars["class_name"])) {
    source_vars["field_type"].replace(
        source_vars["field_type"].indexOf(source_vars["class_name"]),
        source_vars["class_name"].size(), "");

    while (   source_vars["field_type"].startsWith('_')
           || source_vars["field_type"].startsWith(':')) {
      source_vars["field_type"] = source_vars["field_type"].mid(1);
    }
  }

  source_vars["field_name_lower_camel"] = make_lower_camel_name(descriptor->name());
  source_vars["field_name_upper_camel"] = make_upper_camel_name(descriptor->name());

  source_vars["field_name_lower_snake"] = make_lower_snake_name(descriptor->name());
  source_vars["field_name_upper_snake"] = make_upper_snake_name(descriptor->name());

  source_vars["field_name_minor_snake"] = make_minor_snake_name(descriptor->name());

  make_source_split_if_necessary();
  require_source_split();

  if (!descriptor->is_repeated()) {
    incr_source_line("$field_rype$ $class_name$::$field_name_lower_camel$() const {",
                     source_vars);

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_ENUM   ) {
      make_source_line("return static_cast<$field_type$>(d_->d_.$field_name_minor_snake$());",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES  ) {
      make_source_line("return QByteArray::fromStdString(d_->d_.$field_name_minor_snake$());",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_STRING ) {
      make_source_line("return QString" "::fromStdString(d_->d_.$field_name_minor_snake$());",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      make_source_line("return $field_type$(d_->d_.$field_name_minor_snake$());",
                       source_vars);
    }
    else {
      make_source_line("return d_->d_.$field_name_lower_snake$();",
                       source_vars);
    }

    decr_source_line("}");
    make_source_line();
    // ----------

    incr_source_line("bool $class_name$::has$field_name_upper_camel$() const {",
                     source_vars);

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_ENUM)    {
      if        (descriptor_->syntax() == gp::FileDescriptor::SYNTAX_PROTO3) {
        make_source_line("return $field_type$_IsValid($field_name_lower_camel$());",
                         source_vars);
      } else if (descriptor_->syntax() == gp::FileDescriptor::SYNTAX_PROTO2) {
        make_source_line("return d_->d_.has_$field_name_minor_snake$();",
                         source_vars);
      } else {
        make_source_line("return true;",
                         source_vars);

        Q_ASSERT(false);
      }
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES
          || descriptor->type() == gp::FieldDescriptor::TYPE_STRING ) {
      make_source_line("return !d_->d_.$field_name_minor_snake$().empty();",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      make_source_line("return d_->d_.has_$field_name_minor_snake$();",
                       source_vars);
    }
    else {
      make_source_line("return true;");
    }

    decr_source_line("}");
    make_source_line();
    // ----------

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES
          || descriptor->type() == gp::FieldDescriptor::TYPE_STRING
          || descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      incr_source_line("void $class_name$::set$field_name_upper_camel$(const $field_type$& value) {",
                       source_vars);
    } else {
      incr_source_line("void $class_name$::set$field_name_upper_camel$("    "$field_type$ value) {",
                       source_vars);
    }

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_ENUM   ) {
      make_source_line("d_->d_.set_$field_name_minor_snake$(static_cast<__pqpp__::$field_rype$>(value));",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES
          || descriptor->type() == gp::FieldDescriptor::TYPE_STRING ) {
      make_source_line("d_->d_.set_$field_name_minor_snake$(value.toStdString());",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      make_source_line("(*d_->d_.mutable_$field_name_minor_snake$()) = value.__pqpp__();",
                       source_vars);
    }
    else {
      make_source_line("d_->d_.set_$field_name_minor_snake$(value);",
                       source_vars);
    }

    decr_source_line("}");
    make_source_line();
    // ----------

    incr_source_line("void $class_name$::clear$field_name_upper_camel$() {",
                     source_vars);

    make_source_line("d_->d_.clear_$field_name_minor_snake$();",
                     source_vars);

    decr_source_line("}");
  }
  else {
    incr_source_line("$field_rype_list$ $class_name$::$field_name_lower_camel$()          const {",
                     source_vars);

    make_source_line("$field_rype_list$ value;",
                     source_vars);

    make_source_line();

    incr_source_line("for (int i = 0; i < $field_name_lower_camel$Size(); i ++) {",
                     source_vars);

    make_source_line("value.append($field_name_lower_camel$(i));",
                     source_vars);

    decr_source_line("}");
    make_source_line();

    make_source_line("return value;");

    decr_source_line("}");
    make_source_line();
    // ----------

    source_vars["space_a"] = QString(" ").repeated(source_vars["field_type_list"].indexOf(source_vars["field_type"]));
    source_vars["space_b"] = QString(" ").repeated(source_vars["field_type_list"].size() -source_vars["field_type_list"].indexOf(source_vars["field_type"]) -source_vars["field_type"].size());

    incr_source_line("$space_a$$field_rype$ $space_b$$class_name$::$field_name_lower_camel$(int index) const {",
                     source_vars);

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_ENUM  ) {
      make_source_line("return static_cast<$field_type$>(d_->d_.$field_name_lower_snake$(index));",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES  ) {
      make_source_line("return QByteArray::fromStdString(d_->d_.$field_name_lower_snake$(index));",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_STRING ) {
      make_source_line("return QString" "::fromStdString(d_->d_.$field_name_lower_snake$(index));",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      make_source_line("return $field_type$(d_->d_.$field_name_lower_snake$(index));",
                       source_vars);
    }
    else {
      make_source_line("return d_->d_.$field_name_lower_snake$(index);",
                       source_vars);
    }

    decr_source_line("}");
    make_source_line();
    // ----------

    incr_source_line("bool $class_name$::has$field_name_upper_camel$() const {",
                     source_vars);

    make_source_line("return $field_name_lower_camel$Size() > 0;",
                     source_vars);

    decr_source_line("}");
    make_source_line();
    // ----------

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES
          || descriptor->type() == gp::FieldDescriptor::TYPE_STRING
          || descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      incr_source_line("void $class_name$::set$field_name_upper_camel$(           const $field_type_list$& value) {",
                      source_vars);
    }
    else {
      incr_source_line("void $class_name$::set$field_name_upper_camel$(const $field_type_list$& value) {",
                      source_vars);
    }

    make_source_line("clear$field_name_upper_camel$();",
                     source_vars);

    make_source_line();

    incr_source_line("for (auto& vitem : value) {");

    make_source_line("add$field_name_upper_camel$(vitem);",
                     source_vars);

    decr_source_line("}");

    decr_source_line("}");
    make_source_line();
    // ----------

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES
          || descriptor->type() == gp::FieldDescriptor::TYPE_STRING
          || descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      source_vars["space_a"] = QString(" ").repeated(source_vars["field_type_list"].indexOf(source_vars["field_type"]));
      source_vars["space_b"] = QString(" ").repeated(source_vars["field_type_list"].size() -source_vars["field_type"].size() -source_vars["space_a"].size());

      incr_source_line("void $class_name$::set$field_name_upper_camel$(int index, const $space_a$$field_type$$space_b$& value) {",
                       source_vars);
    }
    else {
      source_vars["space_a"] = QString(" ").repeated(source_vars["field_type_list"].indexOf(source_vars["field_type"]));
      source_vars["space_b"] = QString(" ").repeated(source_vars["field_type_list"].size() -source_vars["field_type"].size() -source_vars["space_a"].size());

      source_vars["space_a"] = QString(" ").repeated(source_vars["field_type_list"].indexOf(source_vars["field_type"])
          -static_cast<int>(strlen("int index, ")) +static_cast<int>(strlen("const ")));

      incr_source_line("void $class_name$::set$field_name_upper_camel$(int index, "    "$space_a$$field_type$$space_b$  value) {",
                       source_vars);
    }

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_ENUM   ) {
      make_source_line("d_->d_.set_$field_name_lower_snake$(index, static_cast<__pqpp__::$field_type$>(value));",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES
          || descriptor->type() == gp::FieldDescriptor::TYPE_STRING ) {
      make_source_line("d_->d_.set_$field_name_lower_snake$(index, value.toStdString());",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      make_source_line("(*d_->d_.mutable_$field_name_lower_snake$(index)) = value.__pqpp__();",
                       source_vars);
    }
    else {
      make_source_line("d_->d_.set_$field_name_lower_snake$(index, value);",
                       source_vars);
    }

    decr_source_line("}");
    make_source_line();
    // ----------

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES
          || descriptor->type() == gp::FieldDescriptor::TYPE_STRING
          || descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      source_vars["space_a"] = QString(" ").repeated(source_vars["field_type_list"].indexOf(source_vars["field_type"]));
      source_vars["space_b"] = QString(" ").repeated(source_vars["field_type_list"].size() -source_vars["field_type"].size() -source_vars["space_a"].size());

      incr_source_line("void $class_name$::add$field_name_upper_camel$(           const $space_a$$field_type$$space_b$& value) {",
                       source_vars);
    }
    else {
      source_vars["space_a"] = QString(" ").repeated(source_vars["field_type_list"].indexOf(source_vars["field_type"]));
      source_vars["space_b"] = QString(" ").repeated(source_vars["field_type_list"].size() -source_vars["field_type"].size() -source_vars["space_a"].size());

      source_vars["space_a"] = QString(" ").repeated(source_vars["field_type_list"].indexOf(source_vars["field_type"])
          -static_cast<int>(strlen("int index, ")) +static_cast<int>(strlen("const ")));

      incr_source_line("void $class_name$::add$field_name_upper_camel$(           "    "$space_a$$field_type$$space_b$  value) {",
                       source_vars);
    }

    if      (descriptor->type() == gp::FieldDescriptor::TYPE_ENUM   ) {
      make_source_line("d_->d_.add_$field_name_lower_snake$(       static_cast<__pqpp__::$field_type$>(value));",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_BYTES
          || descriptor->type() == gp::FieldDescriptor::TYPE_STRING ) {
      make_source_line("d_->d_.add_$field_name_lower_snake$(       value.toStdString());",
                       source_vars);
    }
    else if (descriptor->type() == gp::FieldDescriptor::TYPE_MESSAGE) {
      make_source_line("d_->d_.add_$field_name_lower_snake$();",
                       source_vars);

      make_source_line("set$field_name_upper_camel$($field_name_lower_camel$Size() - 1, value);",
                       source_vars);
    }
    else {
      make_source_line("d_->d_.add_$field_name_lower_snake$(       value);",
                       source_vars);
    }

    decr_source_line("}");
    make_source_line();
    // ----------

    incr_source_line("void $class_name$::clear$field_name_upper_camel$() {",
                     source_vars);

    make_source_line("d_->d_.clear_$field_name_lower_snake$();",
                     source_vars);

    decr_source_line("}");
    make_source_line();
    // ----------

    incr_source_line("int  $class_name$::$field_name_lower_camel$Size() const {",
                     source_vars);

    make_source_line("return d_->d_.$field_name_lower_snake$_size();",
                     source_vars);

    decr_source_line("}");
  }
}

// ----------

void ProtobufQppImpl::make_inject() {
  LVD_LOG_T();

  QString     package = QString::fromStdString(descriptor_->package());
  QStringList nspaces = package.split('.');

  if (!cppoutpath_.isEmpty()) {
    cppoutpath_ = rstrip(cppoutpath_, "/").toString();
    cppoutpath_.append('/');
  }

  if (!nspaces.isEmpty()) {
    const QStringList extensions = { ".pb.cc", ".pb.h" };

    for (const QString&    extension : extensions) {
      const QString path = cppoutpath_
                         + QFileInfo(header_name_).baseName()
                         + extension;

      QFile     rfile(path);
      rfile.open(QIODevice:: ReadOnly);

      if (!rfile.isOpen()) {
        LVD_LOGBUF message;
        LVD_LOG_W(&message) << "cannot open"
                            << path
                            << "for reading";

        LVD_THROW_RUNTIME(message);
      }

      QSaveFile wfile(path);
      wfile.open(QIODevice::WriteOnly);

      if (!wfile.isOpen()) {
        LVD_LOGBUF message;
        LVD_LOG_W(&message) << "cannot open"
                            << path
                            << "for writing";

        LVD_THROW_RUNTIME(message);
      }

      bool inside = false;
      int  icount = 0;

      QByteArray cur_namespace = package.toUtf8();
      cur_namespace.replace(".", "::");

      QByteArray new_namespace = cur_namespace;
      new_namespace.append("::__pqpp__");

      while (!rfile.atEnd()) {
        QByteArray line = rfile.readLine();
        line.replace(cur_namespace, new_namespace);

        if (!inside) {
          wfile.write(line);

          if (line == QString(      "namespace %1 {\n").arg(nspaces[icount])) {
            icount ++;

            if (icount == nspaces.size()) {
              inside = true;
              icount = 0;

              wfile.write("namespace __pqpp__ {\n");
            }
          } else {
            inside = false;
            icount = 0;
          }
        } else {
          if (line == QString("}  // namespace %1\n"  ).arg(nspaces.last())) {
            inside = false;
            icount = 0;

            wfile.write("}  // namespace __pqpp__\n");
          }

          wfile.write(line);
        }
      }

      rfile.close();
      wfile.commit();
    }
  }
}

// ----------

void ProtobufQppImpl::make_header_line(QString message, const ProtobufQppImpl::HeaderVars &vars) {
  if (!message.endsWith('\n'))
    message.append('\n');

  std::map<std::string, std::string> vmap;
  for (auto it  = vars.constKeyValueBegin();
            it != vars.constKeyValueEnd()  ;
            it ++) {
    std::string key = (*it).first .toStdString();
    std::string val = (*it).second.toStdString();
    vmap.insert_or_assign(key, val);
  }

  header_writer_->Print(vmap, message.toLocal8Bit().data());
}

void ProtobufQppImpl::make_source_line(QString message, const ProtobufQppImpl::SourceVars &vars) {
  if (!message.endsWith('\n'))
    message.append('\n');

  std::map<std::string, std::string> vmap;
  for (auto it  = vars.constKeyValueBegin();
            it != vars.constKeyValueEnd()  ;
            it ++) {
    std::string key = (*it).first .toStdString();
    std::string val = (*it).second.toStdString();
    vmap.insert_or_assign(key, val);
  }

  source_writer_->Print(vmap, message.toLocal8Bit().data());
}

// ----------

void ProtobufQppImpl::make_namespace_setup(bool source) {
  LVD_LOG_T();

  QString     package = QString::fromStdString(descriptor_->package());
  QStringList nspaces = package.split('.');

  for (auto nspace  = nspaces.cbegin();
            nspace != nspaces.cend();
            nspace ++) {
    if (!source) {
      make_header_line("namespace $namespace$ {",
                       "namespace", *nspace);
    } else {
      make_source_line("namespace $namespace$ {",
                       "namespace", *nspace);
    }
  }
}

void ProtobufQppImpl::make_namespace_close(bool source) {
  LVD_LOG_T();

  QString     package = QString::fromStdString(descriptor_->package());
  QStringList nspaces = package.split('.');

  for (auto nspace  = nspaces.crbegin();
            nspace != nspaces.crend();
            nspace ++) {
    if (!source) {
      make_header_line("}  // namespace $namespace$",
                       "namespace", *nspace);
    } else {
      make_source_line("}  // namespace $namespace$",
                       "namespace", *nspace);
    }
  }
}

}  // namespace lvd::pqpp
