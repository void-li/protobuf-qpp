/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>
#include <utility>

#include <google/protobuf/compiler/code_generator.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/io/printer.h>
#include <google/protobuf/io/zero_copy_stream.h>

#include <QByteArray>
#include <QHash>
#include <QSet>
#include <QString>

#include "lvd/logger.hpp"

#include "namespaces.hpp"  // IWYU pragma: keep

// ----------

namespace {

template <class Type>
auto generate_xxxxxx_data         (const Type   & data) {
  return                 data;
}

template <>
auto generate_xxxxxx_data<QString>(const QString& data) {
  return                 data .toStdString();
}

template <>
auto generate_xxxxxx_data<int>    (const int    & data) {
  return QString::number(data).toStdString();
}

}  // namespace

// ----------

namespace lvd::pqpp {

class ProtobufQppImpl {
 public: LVD_LOGGER

 public:
  ProtobufQppImpl(const gp ::FileDescriptor*   descriptor,
                        gpc::GeneratorContext* gencontext,
                  const QString&               cppoutpath);

  ~ProtobufQppImpl();

 public:
  void protobufQppImpl();

 private:
  void make_header();

  void make_header_includes();
  void make_header_includes(const gp::    Descriptor* descriptor);

  void make_header_declares();
  void make_header_declares(const gp::    Descriptor* descriptor);
  void make_header_declare (const gp::    Descriptor* descriptor);

  void make_header_enumeras();
  void make_header_enumeras(const gp::    Descriptor* descriptor);
  void make_header_enumera (const gp::EnumDescriptor* descriptor);

  void make_header_messages();
  void make_header_messages(const gp::    Descriptor* descriptor);
  void make_header_message (const gp::    Descriptor* descriptor);

  void make_header_message_scope(const QString& scope);

  void make_header_message_fields(const gp::     Descriptor* descriptor);
  void make_header_message_field (const gp::FieldDescriptor* descriptor);

 private:
  void make_source();

  void make_source_includes();

  void make_source_enumeras();
  void make_source_enumeras(const gp::    Descriptor* descriptor);
  void make_source_enumera (const gp::EnumDescriptor* descriptor);

  void make_source_messages();
  void make_source_messages(const gp::    Descriptor* descriptor);
  void make_source_message (const gp::    Descriptor* descriptor);

  void make_source_message_scope(const QString& scope);

  void make_source_message_fields(const gp::     Descriptor* descriptor);
  void make_source_message_field (const gp::FieldDescriptor* descriptor);

 private:
  void make_namespace_setup(bool source = false);
  void make_namespace_close(bool source = false);

 private:
  void make_inject();

 private:
  template <class... Args>
  void make_header_line(QString message, Args... args) {
    if (!message.endsWith('\n'))
      message.append('\n');

    header_writer_->Print(message.toLocal8Bit().data(),
                          generate_xxxxxx_data(args)...);
  }

  using HeaderVars = QHash<QString, QString>;
  void make_header_line(QString message, const HeaderVars& vars = {});

  void make_header_line() {
    make_header_line("");
  }

  template <class... Args>
  void incr_header_line(const QString& message, Args... args) {
    if (!message.isEmpty()) {
      if constexpr (sizeof...(args) == 1) {
        make_header_line         (message, std::forward<Args>(args)...);
      } else {
        make_header_line<Args...>(message, std::forward<Args>(args)...);
      }
    }

    header_writer_-> Indent();
  }

  void incr_header_line() {
    incr_header_line("");
  }

  template <class... Args>
  void decr_header_line(const QString& message, Args... args) {
    header_writer_->Outdent();

    if (!message.isEmpty()) {
      if constexpr (sizeof...(args) == 1) {
        make_header_line         (message, std::forward<Args>(args)...);
      } else {
        make_header_line<Args...>(message, std::forward<Args>(args)...);
      }
    }
  }

  void decr_header_line() {
    decr_header_line("");
  }

  void make_header_space() {
    make_header_line();
  }
  void make_header_space_if_necessary() {
    if (header_space_necessary_) {
      header_space_necessary_ = false;
      make_header_space();
    }
  }
  void require_header_space() {
    header_space_necessary_ = true;
  }

  void make_header_split() {
    make_header_space();
    make_header_line("// ----------");
    make_header_space();
  }
  void make_header_split_if_necessary() {
    if (header_split_necessary_) {
      header_split_necessary_ = false;
      make_header_split();
    }
  }
  void require_header_split() {
    header_split_necessary_ = true;
  }

 private:

  template <class... Args>
  void make_source_line(QString message, Args... args) {
    if (!message.endsWith('\n'))
      message.append('\n');

    source_writer_->Print(message.toLocal8Bit().data(),
                          generate_xxxxxx_data(args)...);
  }

  using SourceVars = QHash<QString, QString>;
  void make_source_line(QString message, const SourceVars& vars = {});

  void make_source_line() {
    make_source_line("");
  }

  template <class... Args>
  void incr_source_line(const QString& message, Args... args) {
    if (!message.isEmpty()) {
      if constexpr (sizeof...(args) == 1) {
        make_source_line         (message, std::forward<Args>(args)...);
      } else {
        make_source_line<Args...>(message, std::forward<Args>(args)...);
      }
    }

    source_writer_-> Indent();
  }

  void incr_source_line() {
    incr_source_line("");
  }

  template <class... Args>
  void decr_source_line(const QString& message, Args... args) {
    source_writer_->Outdent();

    if (!message.isEmpty()) {
      if constexpr (sizeof...(args) == 1) {
        make_source_line         (message, std::forward<Args>(args)...);
      } else {
        make_source_line<Args...>(message, std::forward<Args>(args)...);
      }
    }
  }

  void decr_source_line() {
    decr_source_line("");
  }

  void make_source_space() {
    make_source_line();
  }
  void make_source_space_if_necessary() {
    if (source_space_necessary_) {
      source_space_necessary_ = false;
      make_source_space();
    }
  }
  void require_source_space() {
    source_space_necessary_ = true;
  }

  void make_source_split() {
    make_source_space();
    make_source_line("// ----------");
    make_source_space();
  }
  void make_source_split_if_necessary() {
    if (source_split_necessary_) {
      source_split_necessary_ = false;
      make_source_split();
    }
  }
  void require_source_split() {
    source_split_necessary_ = true;
  }

 private:
  QString header_name_;
  QString header_name_pb_;

  std::unique_ptr<gpi::ZeroCopyOutputStream> header_stream_;
  std::unique_ptr<gpi::Printer>              header_writer_;

  QSet<QString> header_includes_;

  bool header_split_necessary_ = false;
  bool header_space_necessary_ = false;

 private:
  QString source_name_;
  QString source_name_pb_;

  std::unique_ptr<gpi::ZeroCopyOutputStream> source_stream_;
  std::unique_ptr<gpi::Printer>              source_writer_;

  QSet<QString> source_includes_;

  bool source_split_necessary_ = false;
  bool source_space_necessary_ = false;

 private:
  const gp ::FileDescriptor*   descriptor_;
        gpc::GeneratorContext* gencontext_;

  QString cppoutpath_;
};

}  // namespace lvd::pqpp
