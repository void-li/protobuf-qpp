/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <string>

#include <google/protobuf/compiler/code_generator.h>
#include <google/protobuf/descriptor.h>

#include "namespaces.hpp"  // IWYU pragma: keep

// ----------

namespace lvd::pqpp {

class ProtobufQppMain : public gpc::CodeGenerator {
 public:
  bool Generate(const gp ::FileDescriptor*   file,
                const std::string&           parameter,
                      gpc::GeneratorContext* generator_context,
                      std::string*           error) const override;
};

}  // namespace lvd::pqpp
