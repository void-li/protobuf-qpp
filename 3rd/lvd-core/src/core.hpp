/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstddef>           // IWYU pragma: export
#include <initializer_list>  // IWYU pragma: export
#include <stdexcept>         // IWYU pragma: export
#include <string>

#include <type_traits>       // IWYU pragma: export

#include <QtGlobal>          // IWYU pragma: export
#include <QtNumeric>         // IWYU pragma: export

#include <QString>
#include <QStringBuilder>    // IWYU pragma: export

#include <QTextStream>       // IWYU pragma: export

// ----------

namespace lvd {

#define LVD_STR(...) LVD_STR_IMPL(#__VA_ARGS__)
#define LVD_STR_IMPL(...)          __VA_ARGS__

// ----------

#define LVD_UNIQUE_NAME                 LVD_UNIQUE_NAME_IMPL_A(__lvd__unique__, __COUNTER__, __)
#define LVD_UNIQUE_NAME_IMPL_A(A, B, C) LVD_UNIQUE_NAME_IMPL_B(A, B, C)
#define LVD_UNIQUE_NAME_IMPL_B(A, B, C)                        A##B##C

// ----------

template <class F>
class finally {
 public:
  finally(F f) noexcept : f_(f) {}
  ~finally() noexcept { f_(); }

 private:
  F f_;
};

#define LVD_FINALLY lvd::finally LVD_UNIQUE_NAME = [&]

// ----------

namespace __impl__ {

template <class E> [[noreturn]]
inline void throw_throw(const char* func, int line,
                        const std::string& message) {
  throw E(std::string(func)
              .append(":" )
              .append(std::to_string(line))
              .append(": ")
              .append(message));
}

template <class T>
inline std::string throw_tostr         (const T      & str) {
  return std::string(str);
}

template <>
inline std::string throw_tostr<QString>(const QString& str) {
  return str.toStdString();
}

}  // namespace __impl__

#define LVD_THROW(E, M)                                        \
  lvd::__impl__::throw_throw<E>(__PRETTY_FUNCTION__, __LINE__, \
                                lvd::__impl__::throw_tostr(M))

#define LVD_THROW_LOGIC(M) LVD_THROW(std::logic_error, M)
#define LVD_THROW_ORANGE(M) LVD_THROW(std::out_of_range, M)
#define LVD_THROW_RUNTIME(M) LVD_THROW(std::runtime_error, M)
#define LVD_THROW_IMPOSSIBLE                                  \
  LVD_THROW(std::runtime_error, "This should be impossible. " \
                                "You are most likely doomed.")

// ----------

#define LVD_DEFAULT                                                \
  _Pragma("clang diagnostic push")                                 \
  _Pragma("clang diagnostic ignored \"-Wcovered-switch-default\"") \
    default

#define LVD_DEFAULT_BREAK                                          \
  _Pragma("clang diagnostic pop")                                  \
    break

// ----------

namespace __impl__ {

extern QTextStream* qStdOutPtr;
extern QTextStream* qStdErrPtr;

}  // namespace __impl__

QTextStream& qStdOut();
QTextStream& qStdErr();

}  // namespace lvd
