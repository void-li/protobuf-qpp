/**
 * Copyright © 2019 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <utility>

#include <QByteArray>
#include <QDir>
#include <QFile>
#include <QIODevice>
#include <QObject>
#include <QString>

#include "empty.pb.h"                     // IWYU pragma: keep
#include "empty.pb.hpp"                   // IWYU pragma: keep

#include "enumy.pb.h"                     // IWYU pragma: keep
#include "enumy.pb.hpp"                   // IWYU pragma: keep

#include "message.pb.h"                   // IWYU pragma: keep
#include "message.pb.hpp"                 // IWYU pragma: keep

#include "message_with_enum.pb.h"         // IWYU pragma: keep
#include "message_with_enum.pb.hpp"       // IWYU pragma: keep

#include "message_with_some_data.pb.h"    // IWYU pragma: keep
#include "message_with_some_data.pb.hpp"  // IWYU pragma: keep

#include "message_with_full_data.pb.h"    // IWYU pragma: keep
#include "message_with_full_data.pb.hpp"  // IWYU pragma: keep

#include "message_groups.pb.h"            // IWYU pragma: keep
#include "message_groups.pb.hpp"          // IWYU pragma: keep

#include "message_repeat.pb.h"            // IWYU pragma: keep
#include "message_repeat.pb.hpp"          // IWYU pragma: keep

#include "message_camel.pb.h"             // IWYU pragma: keep
#include "message_camel.pb.hpp"           // IWYU pragma: keep

#include "message_snake.pb.h"             // IWYU pragma: keep
#include "message_snake.pb.hpp"           // IWYU pragma: keep

#include "nested_message.pb.h"            // IWYU pragma: keep
#include "nested_message.pb.hpp"          // IWYU pragma: keep

#include "nested_nested_message.pb.h"     // IWYU pragma: keep
#include "nested_nested_message.pb.hpp"   // IWYU pragma: keep

#include "weird_innertype.pb.h"           // IWYU pragma: keep
#include "weird_innertype.pb.hpp"         // IWYU pragma: keep

#include "weird_namespace.pb.h"           // IWYU pragma: keep
#include "weird_namespace.pb.hpp"         // IWYU pragma: keep

#include "test_config.hpp"

#include "lvd/test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

#define COMPILE if (!compile(QTest::currentTestFunction())) return
#define COMPARE if (!compare(QTest::currentTestFunction())) return

// ----------

class ProtobufQppTest;  // IWYU pragma: keep - just don't ask ...
class ProtobufQppTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void empty() {
    COMPILE;
    COMPARE;
  }

  void enumy() {
    COMPILE;
    COMPARE;

    QCOMPARE(li_void::y::Mera::MERA,
             li_void::y::__pqpp__::Mera::MERA);

    QCOMPARE(li_void::y::Mera::LUNA,
             li_void::y::__pqpp__::Mera::LUNA);

    QCOMPARE(li_void::y::Mera_IsValid(li_void::y::Mera::MERA), true );
    QCOMPARE(li_void::y::Mera_IsValid(li_void::y::Mera::LUNA), true );

    QCOMPARE(li_void::y::Mera_IsValid(42)                    , false);
  }

  void message() {
    COMPILE;
    COMPARE;

    li_void::m   ::Mera mera00;

    QByteArray data = mera00.serialize();
    QVERIFY(data.isEmpty());

    bool ok;

    li_void::m   ::Mera mera01(data,                 &ok);
    QVERIFY(ok);

    li_void::m   ::Mera mera02(data, 0, data.size(), &ok);
    QVERIFY(ok);

    li_void::m   ::Mera mera03(          mera00 );
    li_void::m   ::Mera mera04(std::move(mera03));

    li_void::m   ::Mera mera05;
    mera05 =           mera00;

    li_void::m   ::Mera mera06;
    mera06 = std::move(mera05);

    li_void::m   ::__pqpp__::Mera mera07 = mera00.__pqpp__();
    li_void::m   ::          Mera mera08(mera07);
  }

  void message_with_enum() {
    COMPILE;
    COMPARE;

    QCOMPARE(li_void::mwe::Mera::Luna::MERA,
             li_void::mwe::__pqpp__::Mera_Luna::Mera_Luna_MERA);

    QCOMPARE(li_void::mwe::Mera::Luna::LUNA,
             li_void::mwe::__pqpp__::Mera_Luna::Mera_Luna_LUNA);

    QCOMPARE(li_void::mwe::Mera::Luna_IsValid(li_void::y::Mera::MERA), true );
    QCOMPARE(li_void::mwe::Mera::Luna_IsValid(li_void::y::Mera::LUNA), true );

    QCOMPARE(li_void::mwe::Mera::Luna_IsValid(42)                    , false);

    li_void::mwe ::Mera mera00;

    QVERIFY (mera00.hasLuna());
    QCOMPARE(mera00.luna(), li_void::mwe ::Mera::MERA);

    mera00.setLuna(li_void::mwe ::Mera::MERA);

    QVERIFY (mera00.hasLuna());
    QCOMPARE(mera00.luna(), li_void::mwe ::Mera::MERA);

    mera00.setLuna(li_void::mwe ::Mera::LUNA);

    QVERIFY (mera00.hasLuna());
    QCOMPARE(mera00.luna(), li_void::mwe ::Mera::LUNA);

    li_void::mwe ::Mera mera01 = mera00;

    QVERIFY (mera01.hasLuna());
    QCOMPARE(mera01.luna(), li_void::mwe ::Mera::LUNA);

    mera01.setLuna(li_void::mwe::Mera::MERA);

    QVERIFY (mera01.hasLuna());
    QCOMPARE(mera01.luna(), li_void::mwe ::Mera::MERA);

    QVERIFY (mera00.hasLuna());
    QCOMPARE(mera00.luna(), li_void::mwe ::Mera::LUNA);

    mera01 = mera00;

    QVERIFY (mera01.hasLuna());
    QCOMPARE(mera01.luna(), li_void::mwe ::Mera::LUNA);

    QByteArray data = mera00.serialize();
    QVERIFY(!data.isEmpty());

    li_void::mwe ::Mera mera02(data);

    QVERIFY (mera02.hasLuna());
    QCOMPARE(mera02.luna(), li_void::mwe ::Mera::LUNA);

    mera02.clearLuna();
  }

  void message_with_some_datA() {
    COMPILE;
    COMPARE;

    li_void::mwsd::Mera mera00;

    QVERIFY(!mera00.hasMera());
    QCOMPARE(mera00.mera(), "");

    mera00.setMera("Hello World!");

    QVERIFY (mera00.hasMera());
    QCOMPARE(mera00.mera(), "Hello World!");

    li_void::mwsd::Mera mera01 = mera00;

    QVERIFY (mera01.hasMera());
    QCOMPARE(mera01.mera(), "Hello World!");

    mera01.setMera("World Hello!");

    QVERIFY (mera01.hasMera());
    QCOMPARE(mera01.mera(), "World Hello!");

    QVERIFY (mera00.hasMera());
    QCOMPARE(mera00.mera(), "Hello World!");

    mera01 = mera00;

    QVERIFY (mera01.hasMera());
    QCOMPARE(mera01.mera(), "Hello World!");

    QByteArray data = mera00.serialize();
    QVERIFY(!data.isEmpty());

    li_void::mwsd::Mera mera02(data);

    QVERIFY (mera02.hasMera());
    QCOMPARE(mera02.mera(), "Hello World!");
  }

#define SINGLE_S(N, T, V)                 \
  QVERIFY (mera.hasSingle_ ## N ());      \
  QCOMPARE(mera.   single_ ## N (), T()); \
  mera.         setSingle_ ## N (V);      \
  QCOMPARE(mera.   single_ ## N (), V)  ; \
  mera.       clearSingle_ ## N ();       \
  QCOMPARE(mera.   single_ ## N (), T())

#define SINGLE_X(N, T, V)                 \
  QVERIFY(!mera.hasSingle_ ## N ());      \
  QCOMPARE(mera.   single_ ## N (), T()); \
  mera.         setSingle_ ## N (V);      \
  QCOMPARE(mera.   single_ ## N (), V)  ; \
  mera.       clearSingle_ ## N ();       \
  QCOMPARE(mera.   single_ ## N (), T())

  void message_with_full_datA() {
    COMPILE;
    COMPARE;

    li_void::mwfd::Mera mera;

    SINGLE_S( int32,  qint32, 42);
    SINGLE_S( int64,  qint64, 42);

    SINGLE_S(uint32, quint32, 42);
    SINGLE_S(uint64, quint64, 42);

    SINGLE_S(sint32,  qint32, 42);
    SINGLE_S(sint64,  qint64, 42);

    SINGLE_S(bool, bool, true);

    SINGLE_S( fixed32, quint32, 42);
    SINGLE_S(sfixed32,  qint32, 42);
    SINGLE_S(float,    float  , 42);

    SINGLE_S( fixed64, quint64, 42);
    SINGLE_S(sfixed64,  qint64, 42);
    SINGLE_S(double,   double , 42);

    SINGLE_X(string, QString   , "Mera Luna");
    SINGLE_X(bytes,  QByteArray, "Mera Luna");
  }

  void message_groups() {
    COMPILE;
    COMPARE;

    li_void::mg  ::Mera mera00;
    mera00.setMera("Mera");

    QByteArray data00 = mera00.serialize();
    QVERIFY(!data00.isEmpty());

    li_void::mg  ::Mera mera01(data00);
    QCOMPARE(mera01.mera(), "Mera");

    li_void::mg  ::Luna luna00;
    luna00.setLuna("Luna");

    QByteArray data01 = luna00.serialize();
    QVERIFY(!data01.isEmpty());

    li_void::mg  ::Luna luna01(data01);
    QCOMPARE(luna01.luna(), "Luna");
  }

#define REPEAT_S(N, T, V)                      \
  QVERIFY(!mera.hasRepeat_ ## N ());           \
  mera.         addRepeat_ ## N (T());         \
  QVERIFY (mera.hasRepeat_ ## N ());           \
  QCOMPARE(mera.   repeat_ ## N ## Size(), 1); \
  QCOMPARE(mera.   repeat_ ## N (0), T());     \
  mera.         setRepeat_ ## N (0, V);        \
  QCOMPARE(mera.   repeat_ ## N (0), V  );     \
  mera.         setRepeat_ ## N (mera.repeat_ ## N ()); \
  mera.       clearRepeat_ ## N () ;           \
  QVERIFY(!mera.hasRepeat_ ## N ())

#define REPEAT_X(N, T, V)                      \
  QVERIFY(!mera.hasRepeat_ ## N ());           \
  mera.         addRepeat_ ## N (T());         \
  QVERIFY (mera.hasRepeat_ ## N ());           \
  QCOMPARE(mera.   repeat_ ## N ## Size(), 1); \
  QCOMPARE(mera.   repeat_ ## N (0), T());     \
  mera.         setRepeat_ ## N (0, V);        \
  QCOMPARE(mera.   repeat_ ## N (0), V  );     \
  mera.         setRepeat_ ## N (mera.repeat_ ## N ()); \
  mera.       clearRepeat_ ## N () ;           \
  QVERIFY(!mera.hasRepeat_ ## N ())

#define REPEAT_Y(N, T, V)                      \
  QVERIFY(!mera.hasRepeat_ ## N ());           \
  mera.         addRepeat_ ## N (T());         \
  QVERIFY (mera.hasRepeat_ ## N ());           \
  QCOMPARE(mera.   repeat_ ## N ## Size(), 1); \
  QCOMPARE(mera.   repeat_ ## N (0).serialize(), T().serialize()); \
  mera.         setRepeat_ ## N (0, V);        \
  QCOMPARE(mera.   repeat_ ## N (0).serialize(), V  .serialize()); \
  mera.         setRepeat_ ## N (mera.repeat_ ## N ()); \
  mera.       clearRepeat_ ## N () ;           \
  QVERIFY(!mera.hasRepeat_ ## N ())

  void message_repeat() {
    COMPILE;
    COMPARE;

    li_void::mr  ::Mera mera;

    REPEAT_S( int32,  qint32, 42);
    REPEAT_S( int64,  qint64, 42);

    REPEAT_S(uint32, quint32, 42);
    REPEAT_S(uint64, quint64, 42);

    REPEAT_S(sint32,  qint32, 42);
    REPEAT_S(sint64,  qint64, 42);

    REPEAT_S(bool, bool, true);

    REPEAT_S( fixed32, quint32, 42);
    REPEAT_S(sfixed32,  qint32, 42);
    REPEAT_S(float,    float  , 42);

    REPEAT_S( fixed64, quint64, 42);
    REPEAT_S(sfixed64,  qint64, 42);
    REPEAT_S(double,   double , 42);

    REPEAT_X(string, QString   , "Mera Luna");
    REPEAT_X(bytes,  QByteArray, "Mera Luna");

    REPEAT_Y(luna, li_void::mr::Luna, li_void::mr::Luna());
    REPEAT_X(hail, li_void::mr::Hail, li_void::mr::Hail());
  }

  void message_camel() {
    COMPILE;
    COMPARE;

    li_void::mc  ::MeraLuna::MoonHail moonhail00;
    moonhail00.setMoonHail("MoonHail"  );

    li_void::mc  ::MeraLuna           meraluna00;
    meraluna00.setMoonHail( moonhail00 );

    QByteArray data = meraluna00.serialize();
    QVERIFY(!data.isEmpty());

    li_void::mc  ::MeraLuna           meraluna01(data);
    li_void::mc  ::MeraLuna::MoonHail moonhail01 = meraluna01.moonHail();

    QCOMPARE(moonhail01.moonHail(),
             "MoonHail");
  }

  void message_snake() {
    COMPILE;
    COMPARE;

    li_void::ms  ::Mera_Luna::Moon_Hail moonhail00;
    moonhail00.setMoon_hail("MoonHail"  );

    li_void::ms  ::Mera_Luna            meraluna00;
    meraluna00.setMoon_hail( moonhail00 );

    QByteArray data = meraluna00.serialize();
    QVERIFY(!data.isEmpty());

    li_void::ms  ::Mera_Luna            meraluna01(data);
    li_void::ms  ::Mera_Luna::Moon_Hail moonhail01 = meraluna01.moon_hail();

    QCOMPARE(moonhail01.moon_hail(),
             "MoonHail");
  }

  void nested_message() {
    COMPILE;
    COMPARE;

    li_void::nm  ::Mera::Luna       luna00;
    luna00.setLuna("Luna");

    li_void::nm  ::Mera             mera00;
    mera00.setLuna(luna00);

    QByteArray data = mera00.serialize();
    QVERIFY(!data.isEmpty());

    li_void::nm  ::Mera             mera01(data);
    li_void::nm  ::Mera::Luna       luna01 = mera01.luna();

    QCOMPARE(luna01.luna(), "Luna");
  }

  void nested_nested_message() {
    COMPILE;
    COMPARE;

    li_void::nnm ::Mera::Luna::Moon moon00;
    moon00.setMoon("Moon");

    li_void::nnm ::Mera::Luna       luna00;
    luna00.setMoon(moon00);

    li_void::nnm ::Mera             mera00;
    mera00.setLuna(luna00);

    QByteArray data = mera00.serialize();
    QVERIFY(!data.isEmpty());

    li_void::nnm ::Mera             mera01(data);
    li_void::nnm ::Mera::Luna       luna01 = mera01.luna();
    li_void::nnm ::Mera::Luna::Moon moon01 = luna01.moon();

    QCOMPARE(moon01.moon(), "Moon");
  }

  void weird_innertype() {
    COMPILE;
    COMPARE;

    li_void::wi  ::Mera::Luna luna00;
    luna00.setLuna("Luna");

    li_void::wi  ::Moon       moon00;
    moon00.setLuna(luna00);

    QByteArray data = moon00.serialize();
    QVERIFY(!data.isEmpty());

    li_void::wi  ::Moon       moon01(data);
    li_void::wi  ::Mera::Luna luna01 = moon01.luna();

    QCOMPARE(luna01.luna(), "Luna");
  }

  void weird_namespace() {
    COMPILE;
    COMPARE;
  }

  // ----------

 private:
  bool compile(const QString& name) {
    bool outcome = false;

    [&] {
      QVERIFY(QDir(".").mkpath("build"));

      int ret;

      ret = execute("protoc", "--cpp_out=build"      , "-I" + Current_Source_Dir(),
                    QString("%2/%1.proto").arg(name.toLower(), Current_Source_Dir()));
      QCOMPARE(ret, 0);

      ret = execute("protoc", "--qpp_out=build:build", "-I" + Current_Source_Dir(),
                    QString("--plugin=protoc-gen-qpp=%2/../src/protobuf-qpp").arg(Current_Binary_Dir()),
                    QString("%2/%1.proto").arg(name.toLower(), Current_Source_Dir()));
      QCOMPARE(ret, 0);

      outcome = true;
    }();

    return outcome;
  }

  bool compare(const QString& name) {
    bool outcome = false;

    [&] {
      QFile generated_header_file(QString("%1/%2.pb.hpp")
          .arg(Current_Binary_Dir()         , name.toLower()));
      QVERIFY(generated_header_file.open(QFile::ReadOnly));
      QString generated_header_data = generated_header_file.readAll();

      QFile reference_header_file(QString("%1/%2.pb.hpp")
          .arg(Current_Source_Dir() + "/ref", name.toLower()));
      QVERIFY(reference_header_file.open(QFile::ReadOnly));
      QString reference_header_data = reference_header_file.readAll();

      QCOMPARE(generated_header_data, reference_header_data);

      QFile generated_source_file(QString("%1/%2.pb.cpp")
          .arg(Current_Binary_Dir()         , name.toLower()));
      QVERIFY(generated_source_file.open(QFile::ReadOnly));
      QString generated_source_data = generated_source_file.readAll();

      QFile reference_source_file(QString("%1/%2.pb.cpp")
          .arg(Current_Source_Dir() + "/ref", name.toLower()));
      QVERIFY(reference_source_file.open(QFile::ReadOnly));
      QString reference_source_data = reference_source_file.readAll();

      QCOMPARE(generated_source_data, reference_source_data);

      outcome = true;
    }();

    return outcome;
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(ProtobufQppTest)
#include "protobuf_qpp_test.moc"  // IWYU pragma: keep
