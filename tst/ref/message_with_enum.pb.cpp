#include <QSharedData>
#include <QtGlobal>

#include "message_with_enum.pb.hpp"
#include "message_with_enum.pb.h"

// ----------

namespace li_void {
namespace mwe {

bool Mera::Luna_IsValid(int value) {
  return __pqpp__::Mera::Luna_IsValid(value);
}

// ----------

class Mera::__Data__ : public QSharedData {
 public:
  __pqpp__::Mera d_;
};

// ----------

Mera::Mera()
    : d_(new __Data__()) {}

Mera::Mera(const QByteArray    & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera::Mera(const QByteArray    & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera::Mera(const __pqpp__::Mera& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

Mera::Mera(const Mera&  value) = default;
Mera::Mera(      Mera&& value) = default;
Mera::~Mera()                  = default;

Mera& Mera::operator= (const Mera&  value) = default;
Mera& Mera::operator= (      Mera&& value) = default;

QByteArray
Mera::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::Mera& Mera::__pqpp__() const {
  return d_->d_;
}

// ----------

Mera::Luna Mera::luna() const {
  return static_cast<Luna>(d_->d_.luna());
}

bool Mera::hasLuna() const {
  return Luna_IsValid(luna());
}

void Mera::setLuna(Luna value) {
  d_->d_.set_luna(static_cast<__pqpp__::Mera::Luna>(value));
}

void Mera::clearLuna() {
  d_->d_.clear_luna();
}

}  // namespace mwe
}  // namespace li_void
