#include <QSharedData>
#include <QtGlobal>

#include "message_camel.pb.hpp"
#include "message_camel.pb.h"

// ----------

namespace li_void {
namespace mc {

class MeraLuna_MoonHail::__Data__ : public QSharedData {
 public:
  __pqpp__::MeraLuna_MoonHail d_;
};

// ----------

MeraLuna_MoonHail::MeraLuna_MoonHail()
    : d_(new __Data__()) {}

MeraLuna_MoonHail::MeraLuna_MoonHail(const QByteArray                 & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

MeraLuna_MoonHail::MeraLuna_MoonHail(const QByteArray                 & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

MeraLuna_MoonHail::MeraLuna_MoonHail(const __pqpp__::MeraLuna_MoonHail& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

MeraLuna_MoonHail::MeraLuna_MoonHail(const MeraLuna_MoonHail&  value) = default;
MeraLuna_MoonHail::MeraLuna_MoonHail(      MeraLuna_MoonHail&& value) = default;
MeraLuna_MoonHail::~MeraLuna_MoonHail()                               = default;

MeraLuna_MoonHail& MeraLuna_MoonHail::operator= (const MeraLuna_MoonHail&  value) = default;
MeraLuna_MoonHail& MeraLuna_MoonHail::operator= (      MeraLuna_MoonHail&& value) = default;

QByteArray
MeraLuna_MoonHail::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::MeraLuna_MoonHail& MeraLuna_MoonHail::__pqpp__() const {
  return d_->d_;
}

// ----------

QString MeraLuna_MoonHail::moonHail() const {
  return QString::fromStdString(d_->d_.moonhail());
}

bool MeraLuna_MoonHail::hasMoonHail() const {
  return !d_->d_.moonhail().empty();
}

void MeraLuna_MoonHail::setMoonHail(const QString& value) {
  d_->d_.set_moonhail(value.toStdString());
}

void MeraLuna_MoonHail::clearMoonHail() {
  d_->d_.clear_moonhail();
}

// ----------

class MeraLuna::__Data__ : public QSharedData {
 public:
  __pqpp__::MeraLuna d_;
};

// ----------

MeraLuna::MeraLuna()
    : d_(new __Data__()) {}

MeraLuna::MeraLuna(const QByteArray        & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

MeraLuna::MeraLuna(const QByteArray        & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

MeraLuna::MeraLuna(const __pqpp__::MeraLuna& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

MeraLuna::MeraLuna(const MeraLuna&  value) = default;
MeraLuna::MeraLuna(      MeraLuna&& value) = default;
MeraLuna::~MeraLuna()                      = default;

MeraLuna& MeraLuna::operator= (const MeraLuna&  value) = default;
MeraLuna& MeraLuna::operator= (      MeraLuna&& value) = default;

QByteArray
MeraLuna::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::MeraLuna& MeraLuna::__pqpp__() const {
  return d_->d_;
}

// ----------

QString MeraLuna::meraLuna() const {
  return QString::fromStdString(d_->d_.meraluna());
}

bool MeraLuna::hasMeraLuna() const {
  return !d_->d_.meraluna().empty();
}

void MeraLuna::setMeraLuna(const QString& value) {
  d_->d_.set_meraluna(value.toStdString());
}

void MeraLuna::clearMeraLuna() {
  d_->d_.clear_meraluna();
}

// ----------

MeraLuna_MoonHail MeraLuna::moonHail() const {
  return MoonHail(d_->d_.moonhail());
}

bool MeraLuna::hasMoonHail() const {
  return d_->d_.has_moonhail();
}

void MeraLuna::setMoonHail(const MoonHail& value) {
  (*d_->d_.mutable_moonhail()) = value.__pqpp__();
}

void MeraLuna::clearMoonHail() {
  d_->d_.clear_moonhail();
}

}  // namespace mc
}  // namespace li_void
