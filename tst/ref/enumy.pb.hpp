#pragma once

// IWYU pragma: no_include "enumy.pb.h"

// ----------

namespace li_void {
namespace y {

enum Mera {
  MERA = 0,
  LUNA = 1,
};

bool Mera_IsValid(int value);

}  // namespace y
}  // namespace li_void
