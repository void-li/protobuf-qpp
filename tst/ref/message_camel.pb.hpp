#pragma once

// IWYU pragma: no_include "message_camel.pb.h"

#include <QByteArray>
#include <QSharedDataPointer>
#include <QString>

// ----------

namespace li_void {
namespace mc {

namespace __pqpp__ {
class MeraLuna_MoonHail;
class MeraLuna;
}  // namespace __pqpp__

// ----------

class MeraLuna_MoonHail {
 public:
  MeraLuna_MoonHail();
  MeraLuna_MoonHail(const QByteArray                 & value,                   bool* is_ok = nullptr);
  MeraLuna_MoonHail(const QByteArray                 & value, int pos, int len, bool* is_ok = nullptr);
  MeraLuna_MoonHail(const __pqpp__::MeraLuna_MoonHail& value);

  MeraLuna_MoonHail(const MeraLuna_MoonHail&  value);
  MeraLuna_MoonHail(      MeraLuna_MoonHail&& value);
  ~MeraLuna_MoonHail();

  MeraLuna_MoonHail& operator= (const MeraLuna_MoonHail&  value);
  MeraLuna_MoonHail& operator= (      MeraLuna_MoonHail&& value);

  QByteArray serialize() const;

  const __pqpp__::MeraLuna_MoonHail& __pqpp__() const;

 public:
  QString moonHail() const;
  bool hasMoonHail() const;
  void setMoonHail(const QString& value);
  void clearMoonHail();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

// ----------

class MeraLuna {
 public:
  using MoonHail = MeraLuna_MoonHail;

 public:
  MeraLuna();
  MeraLuna(const QByteArray        & value,                   bool* is_ok = nullptr);
  MeraLuna(const QByteArray        & value, int pos, int len, bool* is_ok = nullptr);
  MeraLuna(const __pqpp__::MeraLuna& value);

  MeraLuna(const MeraLuna&  value);
  MeraLuna(      MeraLuna&& value);
  ~MeraLuna();

  MeraLuna& operator= (const MeraLuna&  value);
  MeraLuna& operator= (      MeraLuna&& value);

  QByteArray serialize() const;

  const __pqpp__::MeraLuna& __pqpp__() const;

 public:
  QString meraLuna() const;
  bool hasMeraLuna() const;
  void setMeraLuna(const QString& value);
  void clearMeraLuna();

  MoonHail moonHail() const;
  bool hasMoonHail() const;
  void setMoonHail(const MoonHail& value);
  void clearMoonHail();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

}  // namespace mc
}  // namespace li_void
