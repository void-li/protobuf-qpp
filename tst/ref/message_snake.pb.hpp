#pragma once

// IWYU pragma: no_include "message_snake.pb.h"

#include <QByteArray>
#include <QSharedDataPointer>
#include <QString>

// ----------

namespace li_void {
namespace ms {

namespace __pqpp__ {
class Mera_Luna_Moon_Hail;
class Mera_Luna;
}  // namespace __pqpp__

// ----------

class Mera_Luna_Moon_Hail {
 public:
  Mera_Luna_Moon_Hail();
  Mera_Luna_Moon_Hail(const QByteArray                   & value,                   bool* is_ok = nullptr);
  Mera_Luna_Moon_Hail(const QByteArray                   & value, int pos, int len, bool* is_ok = nullptr);
  Mera_Luna_Moon_Hail(const __pqpp__::Mera_Luna_Moon_Hail& value);

  Mera_Luna_Moon_Hail(const Mera_Luna_Moon_Hail&  value);
  Mera_Luna_Moon_Hail(      Mera_Luna_Moon_Hail&& value);
  ~Mera_Luna_Moon_Hail();

  Mera_Luna_Moon_Hail& operator= (const Mera_Luna_Moon_Hail&  value);
  Mera_Luna_Moon_Hail& operator= (      Mera_Luna_Moon_Hail&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera_Luna_Moon_Hail& __pqpp__() const;

 public:
  QString moon_hail() const;
  bool hasMoon_hail() const;
  void setMoon_hail(const QString& value);
  void clearMoon_hail();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

// ----------

class Mera_Luna {
 public:
  using Moon_Hail = Mera_Luna_Moon_Hail;

 public:
  Mera_Luna();
  Mera_Luna(const QByteArray         & value,                   bool* is_ok = nullptr);
  Mera_Luna(const QByteArray         & value, int pos, int len, bool* is_ok = nullptr);
  Mera_Luna(const __pqpp__::Mera_Luna& value);

  Mera_Luna(const Mera_Luna&  value);
  Mera_Luna(      Mera_Luna&& value);
  ~Mera_Luna();

  Mera_Luna& operator= (const Mera_Luna&  value);
  Mera_Luna& operator= (      Mera_Luna&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera_Luna& __pqpp__() const;

 public:
  QString mera_luna() const;
  bool hasMera_luna() const;
  void setMera_luna(const QString& value);
  void clearMera_luna();

  Moon_Hail moon_hail() const;
  bool hasMoon_hail() const;
  void setMoon_hail(const Moon_Hail& value);
  void clearMoon_hail();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

}  // namespace ms
}  // namespace li_void
