#pragma once

// IWYU pragma: no_include "nested_nested_message.pb.h"

#include <QByteArray>
#include <QSharedDataPointer>
#include <QString>

// ----------

namespace li_void {
namespace nnm {

namespace __pqpp__ {
class Mera_Luna_Moon;
class Mera_Luna;
class Mera;
}  // namespace __pqpp__

// ----------

class Mera_Luna_Moon {
 public:
  Mera_Luna_Moon();
  Mera_Luna_Moon(const QByteArray              & value,                   bool* is_ok = nullptr);
  Mera_Luna_Moon(const QByteArray              & value, int pos, int len, bool* is_ok = nullptr);
  Mera_Luna_Moon(const __pqpp__::Mera_Luna_Moon& value);

  Mera_Luna_Moon(const Mera_Luna_Moon&  value);
  Mera_Luna_Moon(      Mera_Luna_Moon&& value);
  ~Mera_Luna_Moon();

  Mera_Luna_Moon& operator= (const Mera_Luna_Moon&  value);
  Mera_Luna_Moon& operator= (      Mera_Luna_Moon&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera_Luna_Moon& __pqpp__() const;

 public:
  QString moon() const;
  bool hasMoon() const;
  void setMoon(const QString& value);
  void clearMoon();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

// ----------

class Mera_Luna {
 public:
  using Moon = Mera_Luna_Moon;

 public:
  Mera_Luna();
  Mera_Luna(const QByteArray         & value,                   bool* is_ok = nullptr);
  Mera_Luna(const QByteArray         & value, int pos, int len, bool* is_ok = nullptr);
  Mera_Luna(const __pqpp__::Mera_Luna& value);

  Mera_Luna(const Mera_Luna&  value);
  Mera_Luna(      Mera_Luna&& value);
  ~Mera_Luna();

  Mera_Luna& operator= (const Mera_Luna&  value);
  Mera_Luna& operator= (      Mera_Luna&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera_Luna& __pqpp__() const;

 public:
  Moon moon() const;
  bool hasMoon() const;
  void setMoon(const Moon& value);
  void clearMoon();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

// ----------

class Mera {
 public:
  using Luna = Mera_Luna;

 public:
  Mera();
  Mera(const QByteArray    & value,                   bool* is_ok = nullptr);
  Mera(const QByteArray    & value, int pos, int len, bool* is_ok = nullptr);
  Mera(const __pqpp__::Mera& value);

  Mera(const Mera&  value);
  Mera(      Mera&& value);
  ~Mera();

  Mera& operator= (const Mera&  value);
  Mera& operator= (      Mera&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera& __pqpp__() const;

 public:
  Luna luna() const;
  bool hasLuna() const;
  void setLuna(const Luna& value);
  void clearLuna();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

}  // namespace nnm
}  // namespace li_void
