#include <QSharedData>
#include <QtGlobal>

#include "message_repeat.pb.hpp"
#include "message_repeat.pb.h"

// ----------

namespace li_void {
namespace mr {

bool Hail_IsValid(int value) {
  return __pqpp__::Hail_IsValid(value);
}

// ----------

class Luna::__Data__ : public QSharedData {
 public:
  __pqpp__::Luna d_;
};

// ----------

Luna::Luna()
    : d_(new __Data__()) {}

Luna::Luna(const QByteArray    & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

Luna::Luna(const QByteArray    & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

Luna::Luna(const __pqpp__::Luna& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

Luna::Luna(const Luna&  value) = default;
Luna::Luna(      Luna&& value) = default;
Luna::~Luna()                  = default;

Luna& Luna::operator= (const Luna&  value) = default;
Luna& Luna::operator= (      Luna&& value) = default;

QByteArray
Luna::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::Luna& Luna::__pqpp__() const {
  return d_->d_;
}

// ----------

class Mera::__Data__ : public QSharedData {
 public:
  __pqpp__::Mera d_;
};

// ----------

Mera::Mera()
    : d_(new __Data__()) {}

Mera::Mera(const QByteArray    & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera::Mera(const QByteArray    & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera::Mera(const __pqpp__::Mera& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

Mera::Mera(const Mera&  value) = default;
Mera::Mera(      Mera&& value) = default;
Mera::~Mera()                  = default;

Mera& Mera::operator= (const Mera&  value) = default;
Mera& Mera::operator= (      Mera&& value) = default;

QByteArray
Mera::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::Mera& Mera::__pqpp__() const {
  return d_->d_;
}

// ----------

QList<qint32> Mera::repeat_int32()          const {
  QList<qint32> value;

  for (int i = 0; i < repeat_int32Size(); i ++) {
    value.append(repeat_int32(i));
  }

  return value;
}

      qint32  Mera::repeat_int32(int index) const {
  return d_->d_.repeat_int32(index);
}

bool Mera::hasRepeat_int32() const {
  return repeat_int32Size() > 0;
}

void Mera::setRepeat_int32(const QList<qint32>& value) {
  clearRepeat_int32();

  for (auto& vitem : value) {
    addRepeat_int32(vitem);
  }
}

void Mera::setRepeat_int32(int index,  qint32   value) {
  d_->d_.set_repeat_int32(index, value);
}

void Mera::addRepeat_int32(            qint32   value) {
  d_->d_.add_repeat_int32(       value);
}

void Mera::clearRepeat_int32() {
  d_->d_.clear_repeat_int32();
}

int  Mera::repeat_int32Size() const {
  return d_->d_.repeat_int32_size();
}

// ----------

QList<qint64> Mera::repeat_int64()          const {
  QList<qint64> value;

  for (int i = 0; i < repeat_int64Size(); i ++) {
    value.append(repeat_int64(i));
  }

  return value;
}

      qint64  Mera::repeat_int64(int index) const {
  return d_->d_.repeat_int64(index);
}

bool Mera::hasRepeat_int64() const {
  return repeat_int64Size() > 0;
}

void Mera::setRepeat_int64(const QList<qint64>& value) {
  clearRepeat_int64();

  for (auto& vitem : value) {
    addRepeat_int64(vitem);
  }
}

void Mera::setRepeat_int64(int index,  qint64   value) {
  d_->d_.set_repeat_int64(index, value);
}

void Mera::addRepeat_int64(            qint64   value) {
  d_->d_.add_repeat_int64(       value);
}

void Mera::clearRepeat_int64() {
  d_->d_.clear_repeat_int64();
}

int  Mera::repeat_int64Size() const {
  return d_->d_.repeat_int64_size();
}

// ----------

QList<quint32> Mera::repeat_uint32()          const {
  QList<quint32> value;

  for (int i = 0; i < repeat_uint32Size(); i ++) {
    value.append(repeat_uint32(i));
  }

  return value;
}

      quint32  Mera::repeat_uint32(int index) const {
  return d_->d_.repeat_uint32(index);
}

bool Mera::hasRepeat_uint32() const {
  return repeat_uint32Size() > 0;
}

void Mera::setRepeat_uint32(const QList<quint32>& value) {
  clearRepeat_uint32();

  for (auto& vitem : value) {
    addRepeat_uint32(vitem);
  }
}

void Mera::setRepeat_uint32(int index,  quint32   value) {
  d_->d_.set_repeat_uint32(index, value);
}

void Mera::addRepeat_uint32(            quint32   value) {
  d_->d_.add_repeat_uint32(       value);
}

void Mera::clearRepeat_uint32() {
  d_->d_.clear_repeat_uint32();
}

int  Mera::repeat_uint32Size() const {
  return d_->d_.repeat_uint32_size();
}

// ----------

QList<quint64> Mera::repeat_uint64()          const {
  QList<quint64> value;

  for (int i = 0; i < repeat_uint64Size(); i ++) {
    value.append(repeat_uint64(i));
  }

  return value;
}

      quint64  Mera::repeat_uint64(int index) const {
  return d_->d_.repeat_uint64(index);
}

bool Mera::hasRepeat_uint64() const {
  return repeat_uint64Size() > 0;
}

void Mera::setRepeat_uint64(const QList<quint64>& value) {
  clearRepeat_uint64();

  for (auto& vitem : value) {
    addRepeat_uint64(vitem);
  }
}

void Mera::setRepeat_uint64(int index,  quint64   value) {
  d_->d_.set_repeat_uint64(index, value);
}

void Mera::addRepeat_uint64(            quint64   value) {
  d_->d_.add_repeat_uint64(       value);
}

void Mera::clearRepeat_uint64() {
  d_->d_.clear_repeat_uint64();
}

int  Mera::repeat_uint64Size() const {
  return d_->d_.repeat_uint64_size();
}

// ----------

QList<qint32> Mera::repeat_sint32()          const {
  QList<qint32> value;

  for (int i = 0; i < repeat_sint32Size(); i ++) {
    value.append(repeat_sint32(i));
  }

  return value;
}

      qint32  Mera::repeat_sint32(int index) const {
  return d_->d_.repeat_sint32(index);
}

bool Mera::hasRepeat_sint32() const {
  return repeat_sint32Size() > 0;
}

void Mera::setRepeat_sint32(const QList<qint32>& value) {
  clearRepeat_sint32();

  for (auto& vitem : value) {
    addRepeat_sint32(vitem);
  }
}

void Mera::setRepeat_sint32(int index,  qint32   value) {
  d_->d_.set_repeat_sint32(index, value);
}

void Mera::addRepeat_sint32(            qint32   value) {
  d_->d_.add_repeat_sint32(       value);
}

void Mera::clearRepeat_sint32() {
  d_->d_.clear_repeat_sint32();
}

int  Mera::repeat_sint32Size() const {
  return d_->d_.repeat_sint32_size();
}

// ----------

QList<qint64> Mera::repeat_sint64()          const {
  QList<qint64> value;

  for (int i = 0; i < repeat_sint64Size(); i ++) {
    value.append(repeat_sint64(i));
  }

  return value;
}

      qint64  Mera::repeat_sint64(int index) const {
  return d_->d_.repeat_sint64(index);
}

bool Mera::hasRepeat_sint64() const {
  return repeat_sint64Size() > 0;
}

void Mera::setRepeat_sint64(const QList<qint64>& value) {
  clearRepeat_sint64();

  for (auto& vitem : value) {
    addRepeat_sint64(vitem);
  }
}

void Mera::setRepeat_sint64(int index,  qint64   value) {
  d_->d_.set_repeat_sint64(index, value);
}

void Mera::addRepeat_sint64(            qint64   value) {
  d_->d_.add_repeat_sint64(       value);
}

void Mera::clearRepeat_sint64() {
  d_->d_.clear_repeat_sint64();
}

int  Mera::repeat_sint64Size() const {
  return d_->d_.repeat_sint64_size();
}

// ----------

QList<bool> Mera::repeat_bool()          const {
  QList<bool> value;

  for (int i = 0; i < repeat_boolSize(); i ++) {
    value.append(repeat_bool(i));
  }

  return value;
}

      bool  Mera::repeat_bool(int index) const {
  return d_->d_.repeat_bool(index);
}

bool Mera::hasRepeat_bool() const {
  return repeat_boolSize() > 0;
}

void Mera::setRepeat_bool(const QList<bool>& value) {
  clearRepeat_bool();

  for (auto& vitem : value) {
    addRepeat_bool(vitem);
  }
}

void Mera::setRepeat_bool(int index,  bool   value) {
  d_->d_.set_repeat_bool(index, value);
}

void Mera::addRepeat_bool(            bool   value) {
  d_->d_.add_repeat_bool(       value);
}

void Mera::clearRepeat_bool() {
  d_->d_.clear_repeat_bool();
}

int  Mera::repeat_boolSize() const {
  return d_->d_.repeat_bool_size();
}

// ----------

QList<quint32> Mera::repeat_fixed32()          const {
  QList<quint32> value;

  for (int i = 0; i < repeat_fixed32Size(); i ++) {
    value.append(repeat_fixed32(i));
  }

  return value;
}

      quint32  Mera::repeat_fixed32(int index) const {
  return d_->d_.repeat_fixed32(index);
}

bool Mera::hasRepeat_fixed32() const {
  return repeat_fixed32Size() > 0;
}

void Mera::setRepeat_fixed32(const QList<quint32>& value) {
  clearRepeat_fixed32();

  for (auto& vitem : value) {
    addRepeat_fixed32(vitem);
  }
}

void Mera::setRepeat_fixed32(int index,  quint32   value) {
  d_->d_.set_repeat_fixed32(index, value);
}

void Mera::addRepeat_fixed32(            quint32   value) {
  d_->d_.add_repeat_fixed32(       value);
}

void Mera::clearRepeat_fixed32() {
  d_->d_.clear_repeat_fixed32();
}

int  Mera::repeat_fixed32Size() const {
  return d_->d_.repeat_fixed32_size();
}

// ----------

QList<qint32> Mera::repeat_sfixed32()          const {
  QList<qint32> value;

  for (int i = 0; i < repeat_sfixed32Size(); i ++) {
    value.append(repeat_sfixed32(i));
  }

  return value;
}

      qint32  Mera::repeat_sfixed32(int index) const {
  return d_->d_.repeat_sfixed32(index);
}

bool Mera::hasRepeat_sfixed32() const {
  return repeat_sfixed32Size() > 0;
}

void Mera::setRepeat_sfixed32(const QList<qint32>& value) {
  clearRepeat_sfixed32();

  for (auto& vitem : value) {
    addRepeat_sfixed32(vitem);
  }
}

void Mera::setRepeat_sfixed32(int index,  qint32   value) {
  d_->d_.set_repeat_sfixed32(index, value);
}

void Mera::addRepeat_sfixed32(            qint32   value) {
  d_->d_.add_repeat_sfixed32(       value);
}

void Mera::clearRepeat_sfixed32() {
  d_->d_.clear_repeat_sfixed32();
}

int  Mera::repeat_sfixed32Size() const {
  return d_->d_.repeat_sfixed32_size();
}

// ----------

QList<float> Mera::repeat_float()          const {
  QList<float> value;

  for (int i = 0; i < repeat_floatSize(); i ++) {
    value.append(repeat_float(i));
  }

  return value;
}

      float  Mera::repeat_float(int index) const {
  return d_->d_.repeat_float(index);
}

bool Mera::hasRepeat_float() const {
  return repeat_floatSize() > 0;
}

void Mera::setRepeat_float(const QList<float>& value) {
  clearRepeat_float();

  for (auto& vitem : value) {
    addRepeat_float(vitem);
  }
}

void Mera::setRepeat_float(int index,  float   value) {
  d_->d_.set_repeat_float(index, value);
}

void Mera::addRepeat_float(            float   value) {
  d_->d_.add_repeat_float(       value);
}

void Mera::clearRepeat_float() {
  d_->d_.clear_repeat_float();
}

int  Mera::repeat_floatSize() const {
  return d_->d_.repeat_float_size();
}

// ----------

QList<quint64> Mera::repeat_fixed64()          const {
  QList<quint64> value;

  for (int i = 0; i < repeat_fixed64Size(); i ++) {
    value.append(repeat_fixed64(i));
  }

  return value;
}

      quint64  Mera::repeat_fixed64(int index) const {
  return d_->d_.repeat_fixed64(index);
}

bool Mera::hasRepeat_fixed64() const {
  return repeat_fixed64Size() > 0;
}

void Mera::setRepeat_fixed64(const QList<quint64>& value) {
  clearRepeat_fixed64();

  for (auto& vitem : value) {
    addRepeat_fixed64(vitem);
  }
}

void Mera::setRepeat_fixed64(int index,  quint64   value) {
  d_->d_.set_repeat_fixed64(index, value);
}

void Mera::addRepeat_fixed64(            quint64   value) {
  d_->d_.add_repeat_fixed64(       value);
}

void Mera::clearRepeat_fixed64() {
  d_->d_.clear_repeat_fixed64();
}

int  Mera::repeat_fixed64Size() const {
  return d_->d_.repeat_fixed64_size();
}

// ----------

QList<qint64> Mera::repeat_sfixed64()          const {
  QList<qint64> value;

  for (int i = 0; i < repeat_sfixed64Size(); i ++) {
    value.append(repeat_sfixed64(i));
  }

  return value;
}

      qint64  Mera::repeat_sfixed64(int index) const {
  return d_->d_.repeat_sfixed64(index);
}

bool Mera::hasRepeat_sfixed64() const {
  return repeat_sfixed64Size() > 0;
}

void Mera::setRepeat_sfixed64(const QList<qint64>& value) {
  clearRepeat_sfixed64();

  for (auto& vitem : value) {
    addRepeat_sfixed64(vitem);
  }
}

void Mera::setRepeat_sfixed64(int index,  qint64   value) {
  d_->d_.set_repeat_sfixed64(index, value);
}

void Mera::addRepeat_sfixed64(            qint64   value) {
  d_->d_.add_repeat_sfixed64(       value);
}

void Mera::clearRepeat_sfixed64() {
  d_->d_.clear_repeat_sfixed64();
}

int  Mera::repeat_sfixed64Size() const {
  return d_->d_.repeat_sfixed64_size();
}

// ----------

QList<double> Mera::repeat_double()          const {
  QList<double> value;

  for (int i = 0; i < repeat_doubleSize(); i ++) {
    value.append(repeat_double(i));
  }

  return value;
}

      double  Mera::repeat_double(int index) const {
  return d_->d_.repeat_double(index);
}

bool Mera::hasRepeat_double() const {
  return repeat_doubleSize() > 0;
}

void Mera::setRepeat_double(const QList<double>& value) {
  clearRepeat_double();

  for (auto& vitem : value) {
    addRepeat_double(vitem);
  }
}

void Mera::setRepeat_double(int index,  double   value) {
  d_->d_.set_repeat_double(index, value);
}

void Mera::addRepeat_double(            double   value) {
  d_->d_.add_repeat_double(       value);
}

void Mera::clearRepeat_double() {
  d_->d_.clear_repeat_double();
}

int  Mera::repeat_doubleSize() const {
  return d_->d_.repeat_double_size();
}

// ----------

QStringList Mera::repeat_string()          const {
  QStringList value;

  for (int i = 0; i < repeat_stringSize(); i ++) {
    value.append(repeat_string(i));
  }

  return value;
}

QString     Mera::repeat_string(int index) const {
  return QString::fromStdString(d_->d_.repeat_string(index));
}

bool Mera::hasRepeat_string() const {
  return repeat_stringSize() > 0;
}

void Mera::setRepeat_string(           const QStringList& value) {
  clearRepeat_string();

  for (auto& vitem : value) {
    addRepeat_string(vitem);
  }
}

void Mera::setRepeat_string(int index, const QString    & value) {
  d_->d_.set_repeat_string(index, value.toStdString());
}

void Mera::addRepeat_string(           const QString    & value) {
  d_->d_.add_repeat_string(       value.toStdString());
}

void Mera::clearRepeat_string() {
  d_->d_.clear_repeat_string();
}

int  Mera::repeat_stringSize() const {
  return d_->d_.repeat_string_size();
}

// ----------

QByteArrayList Mera::repeat_bytes()          const {
  QByteArrayList value;

  for (int i = 0; i < repeat_bytesSize(); i ++) {
    value.append(repeat_bytes(i));
  }

  return value;
}

QByteArray     Mera::repeat_bytes(int index) const {
  return QByteArray::fromStdString(d_->d_.repeat_bytes(index));
}

bool Mera::hasRepeat_bytes() const {
  return repeat_bytesSize() > 0;
}

void Mera::setRepeat_bytes(           const QByteArrayList& value) {
  clearRepeat_bytes();

  for (auto& vitem : value) {
    addRepeat_bytes(vitem);
  }
}

void Mera::setRepeat_bytes(int index, const QByteArray    & value) {
  d_->d_.set_repeat_bytes(index, value.toStdString());
}

void Mera::addRepeat_bytes(           const QByteArray    & value) {
  d_->d_.add_repeat_bytes(       value.toStdString());
}

void Mera::clearRepeat_bytes() {
  d_->d_.clear_repeat_bytes();
}

int  Mera::repeat_bytesSize() const {
  return d_->d_.repeat_bytes_size();
}

// ----------

QList<Luna> Mera::repeat_luna()          const {
  QList<Luna> value;

  for (int i = 0; i < repeat_lunaSize(); i ++) {
    value.append(repeat_luna(i));
  }

  return value;
}

      Luna  Mera::repeat_luna(int index) const {
  return Luna(d_->d_.repeat_luna(index));
}

bool Mera::hasRepeat_luna() const {
  return repeat_lunaSize() > 0;
}

void Mera::setRepeat_luna(           const QList<Luna>& value) {
  clearRepeat_luna();

  for (auto& vitem : value) {
    addRepeat_luna(vitem);
  }
}

void Mera::setRepeat_luna(int index, const       Luna & value) {
  (*d_->d_.mutable_repeat_luna(index)) = value.__pqpp__();
}

void Mera::addRepeat_luna(           const       Luna & value) {
  d_->d_.add_repeat_luna();
  setRepeat_luna(repeat_lunaSize() - 1, value);
}

void Mera::clearRepeat_luna() {
  d_->d_.clear_repeat_luna();
}

int  Mera::repeat_lunaSize() const {
  return d_->d_.repeat_luna_size();
}

// ----------

QList<Hail> Mera::repeat_hail()          const {
  QList<Hail> value;

  for (int i = 0; i < repeat_hailSize(); i ++) {
    value.append(repeat_hail(i));
  }

  return value;
}

      Hail  Mera::repeat_hail(int index) const {
  return static_cast<Hail>(d_->d_.repeat_hail(index));
}

bool Mera::hasRepeat_hail() const {
  return repeat_hailSize() > 0;
}

void Mera::setRepeat_hail(const QList<Hail>& value) {
  clearRepeat_hail();

  for (auto& vitem : value) {
    addRepeat_hail(vitem);
  }
}

void Mera::setRepeat_hail(int index,  Hail   value) {
  d_->d_.set_repeat_hail(index, static_cast<__pqpp__::Hail>(value));
}

void Mera::addRepeat_hail(            Hail   value) {
  d_->d_.add_repeat_hail(       static_cast<__pqpp__::Hail>(value));
}

void Mera::clearRepeat_hail() {
  d_->d_.clear_repeat_hail();
}

int  Mera::repeat_hailSize() const {
  return d_->d_.repeat_hail_size();
}

}  // namespace mr
}  // namespace li_void
