#pragma once

// IWYU pragma: no_include "message.pb.h"

#include <QByteArray>
#include <QSharedDataPointer>

// ----------

namespace li_void {
namespace m {

namespace __pqpp__ {
class Mera;
}  // namespace __pqpp__

// ----------

class Mera {
 public:
  Mera();
  Mera(const QByteArray    & value,                   bool* is_ok = nullptr);
  Mera(const QByteArray    & value, int pos, int len, bool* is_ok = nullptr);
  Mera(const __pqpp__::Mera& value);

  Mera(const Mera&  value);
  Mera(      Mera&& value);
  ~Mera();

  Mera& operator= (const Mera&  value);
  Mera& operator= (      Mera&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera& __pqpp__() const;

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

}  // namespace m
}  // namespace li_void
