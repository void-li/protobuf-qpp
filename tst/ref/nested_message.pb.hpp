#pragma once

// IWYU pragma: no_include "nested_message.pb.h"

#include <QByteArray>
#include <QSharedDataPointer>
#include <QString>

// ----------

namespace li_void {
namespace nm {

namespace __pqpp__ {
class Mera_Luna;
class Mera;
}  // namespace __pqpp__

// ----------

class Mera_Luna {
 public:
  Mera_Luna();
  Mera_Luna(const QByteArray         & value,                   bool* is_ok = nullptr);
  Mera_Luna(const QByteArray         & value, int pos, int len, bool* is_ok = nullptr);
  Mera_Luna(const __pqpp__::Mera_Luna& value);

  Mera_Luna(const Mera_Luna&  value);
  Mera_Luna(      Mera_Luna&& value);
  ~Mera_Luna();

  Mera_Luna& operator= (const Mera_Luna&  value);
  Mera_Luna& operator= (      Mera_Luna&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera_Luna& __pqpp__() const;

 public:
  QString luna() const;
  bool hasLuna() const;
  void setLuna(const QString& value);
  void clearLuna();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

// ----------

class Mera {
 public:
  using Luna = Mera_Luna;

 public:
  Mera();
  Mera(const QByteArray    & value,                   bool* is_ok = nullptr);
  Mera(const QByteArray    & value, int pos, int len, bool* is_ok = nullptr);
  Mera(const __pqpp__::Mera& value);

  Mera(const Mera&  value);
  Mera(      Mera&& value);
  ~Mera();

  Mera& operator= (const Mera&  value);
  Mera& operator= (      Mera&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera& __pqpp__() const;

 public:
  Luna luna() const;
  bool hasLuna() const;
  void setLuna(const Luna& value);
  void clearLuna();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

}  // namespace nm
}  // namespace li_void
