#include <QSharedData>
#include <QtGlobal>

#include "message_with_some_data.pb.hpp"
#include "message_with_some_data.pb.h"

// ----------

namespace li_void {
namespace mwsd {

class Mera::__Data__ : public QSharedData {
 public:
  __pqpp__::Mera d_;
};

// ----------

Mera::Mera()
    : d_(new __Data__()) {}

Mera::Mera(const QByteArray    & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera::Mera(const QByteArray    & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera::Mera(const __pqpp__::Mera& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

Mera::Mera(const Mera&  value) = default;
Mera::Mera(      Mera&& value) = default;
Mera::~Mera()                  = default;

Mera& Mera::operator= (const Mera&  value) = default;
Mera& Mera::operator= (      Mera&& value) = default;

QByteArray
Mera::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::Mera& Mera::__pqpp__() const {
  return d_->d_;
}

// ----------

QString Mera::mera() const {
  return QString::fromStdString(d_->d_.mera());
}

bool Mera::hasMera() const {
  return !d_->d_.mera().empty();
}

void Mera::setMera(const QString& value) {
  d_->d_.set_mera(value.toStdString());
}

void Mera::clearMera() {
  d_->d_.clear_mera();
}

}  // namespace mwsd
}  // namespace li_void
