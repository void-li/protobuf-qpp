#pragma once

// IWYU pragma: no_include "message_with_some_data.pb.h"

#include <QByteArray>
#include <QSharedDataPointer>
#include <QString>

// ----------

namespace li_void {
namespace mwsd {

namespace __pqpp__ {
class Mera;
}  // namespace __pqpp__

// ----------

class Mera {
 public:
  Mera();
  Mera(const QByteArray    & value,                   bool* is_ok = nullptr);
  Mera(const QByteArray    & value, int pos, int len, bool* is_ok = nullptr);
  Mera(const __pqpp__::Mera& value);

  Mera(const Mera&  value);
  Mera(      Mera&& value);
  ~Mera();

  Mera& operator= (const Mera&  value);
  Mera& operator= (      Mera&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera& __pqpp__() const;

 public:
  QString mera() const;
  bool hasMera() const;
  void setMera(const QString& value);
  void clearMera();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

}  // namespace mwsd
}  // namespace li_void
