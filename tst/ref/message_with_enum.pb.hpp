#pragma once

// IWYU pragma: no_include "message_with_enum.pb.h"

#include <QByteArray>
#include <QSharedDataPointer>

// ----------

namespace li_void {
namespace mwe {

namespace __pqpp__ {
class Mera;
}  // namespace __pqpp__

// ----------

class Mera {
 public:
  enum Luna {
    MERA = 0,
    LUNA = 1,
  };

  static bool Luna_IsValid(int value);

 public:
  Mera();
  Mera(const QByteArray    & value,                   bool* is_ok = nullptr);
  Mera(const QByteArray    & value, int pos, int len, bool* is_ok = nullptr);
  Mera(const __pqpp__::Mera& value);

  Mera(const Mera&  value);
  Mera(      Mera&& value);
  ~Mera();

  Mera& operator= (const Mera&  value);
  Mera& operator= (      Mera&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera& __pqpp__() const;

 public:
  Luna luna() const;
  bool hasLuna() const;
  void setLuna(Luna value);
  void clearLuna();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

}  // namespace mwe
}  // namespace li_void
