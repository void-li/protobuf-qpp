#pragma once

// IWYU pragma: no_include "message_groups.pb.h"

#include <QByteArray>
#include <QSharedDataPointer>
#include <QString>

// ----------

namespace li_void {
namespace mg {

namespace __pqpp__ {
class Mera;
class Luna;
}  // namespace __pqpp__

// ----------

class Mera {
 public:
  Mera();
  Mera(const QByteArray    & value,                   bool* is_ok = nullptr);
  Mera(const QByteArray    & value, int pos, int len, bool* is_ok = nullptr);
  Mera(const __pqpp__::Mera& value);

  Mera(const Mera&  value);
  Mera(      Mera&& value);
  ~Mera();

  Mera& operator= (const Mera&  value);
  Mera& operator= (      Mera&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera& __pqpp__() const;

 public:
  QString mera() const;
  bool hasMera() const;
  void setMera(const QString& value);
  void clearMera();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

// ----------

class Luna {
 public:
  Luna();
  Luna(const QByteArray    & value,                   bool* is_ok = nullptr);
  Luna(const QByteArray    & value, int pos, int len, bool* is_ok = nullptr);
  Luna(const __pqpp__::Luna& value);

  Luna(const Luna&  value);
  Luna(      Luna&& value);
  ~Luna();

  Luna& operator= (const Luna&  value);
  Luna& operator= (      Luna&& value);

  QByteArray serialize() const;

  const __pqpp__::Luna& __pqpp__() const;

 public:
  QString luna() const;
  bool hasLuna() const;
  void setLuna(const QString& value);
  void clearLuna();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

}  // namespace mg
}  // namespace li_void
