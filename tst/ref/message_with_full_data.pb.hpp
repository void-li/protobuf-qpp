#pragma once

// IWYU pragma: no_include "message_with_full_data.pb.h"

#include <QByteArray>
#include <QSharedDataPointer>
#include <QString>
#include <QtGlobal>

// ----------

namespace li_void {
namespace mwfd {

namespace __pqpp__ {
class Mera;
}  // namespace __pqpp__

// ----------

class Mera {
 public:
  Mera();
  Mera(const QByteArray    & value,                   bool* is_ok = nullptr);
  Mera(const QByteArray    & value, int pos, int len, bool* is_ok = nullptr);
  Mera(const __pqpp__::Mera& value);

  Mera(const Mera&  value);
  Mera(      Mera&& value);
  ~Mera();

  Mera& operator= (const Mera&  value);
  Mera& operator= (      Mera&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera& __pqpp__() const;

 public:
  qint32 single_int32() const;
  bool hasSingle_int32() const;
  void setSingle_int32(qint32 value);
  void clearSingle_int32();

  qint64 single_int64() const;
  bool hasSingle_int64() const;
  void setSingle_int64(qint64 value);
  void clearSingle_int64();

  quint32 single_uint32() const;
  bool hasSingle_uint32() const;
  void setSingle_uint32(quint32 value);
  void clearSingle_uint32();

  quint64 single_uint64() const;
  bool hasSingle_uint64() const;
  void setSingle_uint64(quint64 value);
  void clearSingle_uint64();

  qint32 single_sint32() const;
  bool hasSingle_sint32() const;
  void setSingle_sint32(qint32 value);
  void clearSingle_sint32();

  qint64 single_sint64() const;
  bool hasSingle_sint64() const;
  void setSingle_sint64(qint64 value);
  void clearSingle_sint64();

  bool single_bool() const;
  bool hasSingle_bool() const;
  void setSingle_bool(bool value);
  void clearSingle_bool();

  quint32 single_fixed32() const;
  bool hasSingle_fixed32() const;
  void setSingle_fixed32(quint32 value);
  void clearSingle_fixed32();

  qint32 single_sfixed32() const;
  bool hasSingle_sfixed32() const;
  void setSingle_sfixed32(qint32 value);
  void clearSingle_sfixed32();

  float single_float() const;
  bool hasSingle_float() const;
  void setSingle_float(float value);
  void clearSingle_float();

  quint64 single_fixed64() const;
  bool hasSingle_fixed64() const;
  void setSingle_fixed64(quint64 value);
  void clearSingle_fixed64();

  qint64 single_sfixed64() const;
  bool hasSingle_sfixed64() const;
  void setSingle_sfixed64(qint64 value);
  void clearSingle_sfixed64();

  double single_double() const;
  bool hasSingle_double() const;
  void setSingle_double(double value);
  void clearSingle_double();

  QString single_string() const;
  bool hasSingle_string() const;
  void setSingle_string(const QString& value);
  void clearSingle_string();

  QByteArray single_bytes() const;
  bool hasSingle_bytes() const;
  void setSingle_bytes(const QByteArray& value);
  void clearSingle_bytes();

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

}  // namespace mwfd
}  // namespace li_void
