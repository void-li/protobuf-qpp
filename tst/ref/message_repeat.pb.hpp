#pragma once

// IWYU pragma: no_include "message_repeat.pb.h"

#include <QByteArray>
#include <QByteArrayList>
#include <QList>
#include <QSharedDataPointer>
#include <QString>
#include <QStringList>
#include <QtGlobal>

// ----------

namespace li_void {
namespace mr {

namespace __pqpp__ {
class Luna;
class Mera;
}  // namespace __pqpp__

// ----------

enum Hail {
  MERA = 0,
  LUNA = 1,
};

bool Hail_IsValid(int value);

// ----------

class Luna {
 public:
  Luna();
  Luna(const QByteArray    & value,                   bool* is_ok = nullptr);
  Luna(const QByteArray    & value, int pos, int len, bool* is_ok = nullptr);
  Luna(const __pqpp__::Luna& value);

  Luna(const Luna&  value);
  Luna(      Luna&& value);
  ~Luna();

  Luna& operator= (const Luna&  value);
  Luna& operator= (      Luna&& value);

  QByteArray serialize() const;

  const __pqpp__::Luna& __pqpp__() const;

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

// ----------

class Mera {
 public:
  Mera();
  Mera(const QByteArray    & value,                   bool* is_ok = nullptr);
  Mera(const QByteArray    & value, int pos, int len, bool* is_ok = nullptr);
  Mera(const __pqpp__::Mera& value);

  Mera(const Mera&  value);
  Mera(      Mera&& value);
  ~Mera();

  Mera& operator= (const Mera&  value);
  Mera& operator= (      Mera&& value);

  QByteArray serialize() const;

  const __pqpp__::Mera& __pqpp__() const;

 public:
  QList<qint32> repeat_int32()          const;
        qint32  repeat_int32(int index) const;

  bool hasRepeat_int32() const;
  void setRepeat_int32(const QList<qint32>& value);
  void setRepeat_int32(int index,  qint32   value);
  void addRepeat_int32(            qint32   value);

  void clearRepeat_int32();
  int  repeat_int32Size() const;

  QList<qint64> repeat_int64()          const;
        qint64  repeat_int64(int index) const;

  bool hasRepeat_int64() const;
  void setRepeat_int64(const QList<qint64>& value);
  void setRepeat_int64(int index,  qint64   value);
  void addRepeat_int64(            qint64   value);

  void clearRepeat_int64();
  int  repeat_int64Size() const;

  QList<quint32> repeat_uint32()          const;
        quint32  repeat_uint32(int index) const;

  bool hasRepeat_uint32() const;
  void setRepeat_uint32(const QList<quint32>& value);
  void setRepeat_uint32(int index,  quint32   value);
  void addRepeat_uint32(            quint32   value);

  void clearRepeat_uint32();
  int  repeat_uint32Size() const;

  QList<quint64> repeat_uint64()          const;
        quint64  repeat_uint64(int index) const;

  bool hasRepeat_uint64() const;
  void setRepeat_uint64(const QList<quint64>& value);
  void setRepeat_uint64(int index,  quint64   value);
  void addRepeat_uint64(            quint64   value);

  void clearRepeat_uint64();
  int  repeat_uint64Size() const;

  QList<qint32> repeat_sint32()          const;
        qint32  repeat_sint32(int index) const;

  bool hasRepeat_sint32() const;
  void setRepeat_sint32(const QList<qint32>& value);
  void setRepeat_sint32(int index,  qint32   value);
  void addRepeat_sint32(            qint32   value);

  void clearRepeat_sint32();
  int  repeat_sint32Size() const;

  QList<qint64> repeat_sint64()          const;
        qint64  repeat_sint64(int index) const;

  bool hasRepeat_sint64() const;
  void setRepeat_sint64(const QList<qint64>& value);
  void setRepeat_sint64(int index,  qint64   value);
  void addRepeat_sint64(            qint64   value);

  void clearRepeat_sint64();
  int  repeat_sint64Size() const;

  QList<bool> repeat_bool()          const;
        bool  repeat_bool(int index) const;

  bool hasRepeat_bool() const;
  void setRepeat_bool(const QList<bool>& value);
  void setRepeat_bool(int index,  bool   value);
  void addRepeat_bool(            bool   value);

  void clearRepeat_bool();
  int  repeat_boolSize() const;

  QList<quint32> repeat_fixed32()          const;
        quint32  repeat_fixed32(int index) const;

  bool hasRepeat_fixed32() const;
  void setRepeat_fixed32(const QList<quint32>& value);
  void setRepeat_fixed32(int index,  quint32   value);
  void addRepeat_fixed32(            quint32   value);

  void clearRepeat_fixed32();
  int  repeat_fixed32Size() const;

  QList<qint32> repeat_sfixed32()          const;
        qint32  repeat_sfixed32(int index) const;

  bool hasRepeat_sfixed32() const;
  void setRepeat_sfixed32(const QList<qint32>& value);
  void setRepeat_sfixed32(int index,  qint32   value);
  void addRepeat_sfixed32(            qint32   value);

  void clearRepeat_sfixed32();
  int  repeat_sfixed32Size() const;

  QList<float> repeat_float()          const;
        float  repeat_float(int index) const;

  bool hasRepeat_float() const;
  void setRepeat_float(const QList<float>& value);
  void setRepeat_float(int index,  float   value);
  void addRepeat_float(            float   value);

  void clearRepeat_float();
  int  repeat_floatSize() const;

  QList<quint64> repeat_fixed64()          const;
        quint64  repeat_fixed64(int index) const;

  bool hasRepeat_fixed64() const;
  void setRepeat_fixed64(const QList<quint64>& value);
  void setRepeat_fixed64(int index,  quint64   value);
  void addRepeat_fixed64(            quint64   value);

  void clearRepeat_fixed64();
  int  repeat_fixed64Size() const;

  QList<qint64> repeat_sfixed64()          const;
        qint64  repeat_sfixed64(int index) const;

  bool hasRepeat_sfixed64() const;
  void setRepeat_sfixed64(const QList<qint64>& value);
  void setRepeat_sfixed64(int index,  qint64   value);
  void addRepeat_sfixed64(            qint64   value);

  void clearRepeat_sfixed64();
  int  repeat_sfixed64Size() const;

  QList<double> repeat_double()          const;
        double  repeat_double(int index) const;

  bool hasRepeat_double() const;
  void setRepeat_double(const QList<double>& value);
  void setRepeat_double(int index,  double   value);
  void addRepeat_double(            double   value);

  void clearRepeat_double();
  int  repeat_doubleSize() const;

  QStringList repeat_string()          const;
  QString     repeat_string(int index) const;

  bool hasRepeat_string() const;
  void setRepeat_string(           const QStringList& value);
  void setRepeat_string(int index, const QString    & value);
  void addRepeat_string(           const QString    & value);

  void clearRepeat_string();
  int  repeat_stringSize() const;

  QByteArrayList repeat_bytes()          const;
  QByteArray     repeat_bytes(int index) const;

  bool hasRepeat_bytes() const;
  void setRepeat_bytes(           const QByteArrayList& value);
  void setRepeat_bytes(int index, const QByteArray    & value);
  void addRepeat_bytes(           const QByteArray    & value);

  void clearRepeat_bytes();
  int  repeat_bytesSize() const;

  QList<Luna> repeat_luna()          const;
        Luna  repeat_luna(int index) const;

  bool hasRepeat_luna() const;
  void setRepeat_luna(           const QList<Luna>& value);
  void setRepeat_luna(int index, const       Luna & value);
  void addRepeat_luna(           const       Luna & value);

  void clearRepeat_luna();
  int  repeat_lunaSize() const;

  QList<Hail> repeat_hail()          const;
        Hail  repeat_hail(int index) const;

  bool hasRepeat_hail() const;
  void setRepeat_hail(const QList<Hail>& value);
  void setRepeat_hail(int index,  Hail   value);
  void addRepeat_hail(            Hail   value);

  void clearRepeat_hail();
  int  repeat_hailSize() const;

 private:
  class              __Data__;
  QSharedDataPointer<__Data__> d_;
};

}  // namespace mr
}  // namespace li_void
