#include <QSharedData>
#include <QtGlobal>

#include "nested_message.pb.hpp"
#include "nested_message.pb.h"

// ----------

namespace li_void {
namespace nm {

class Mera_Luna::__Data__ : public QSharedData {
 public:
  __pqpp__::Mera_Luna d_;
};

// ----------

Mera_Luna::Mera_Luna()
    : d_(new __Data__()) {}

Mera_Luna::Mera_Luna(const QByteArray         & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera_Luna::Mera_Luna(const QByteArray         & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera_Luna::Mera_Luna(const __pqpp__::Mera_Luna& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

Mera_Luna::Mera_Luna(const Mera_Luna&  value) = default;
Mera_Luna::Mera_Luna(      Mera_Luna&& value) = default;
Mera_Luna::~Mera_Luna()                       = default;

Mera_Luna& Mera_Luna::operator= (const Mera_Luna&  value) = default;
Mera_Luna& Mera_Luna::operator= (      Mera_Luna&& value) = default;

QByteArray
Mera_Luna::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::Mera_Luna& Mera_Luna::__pqpp__() const {
  return d_->d_;
}

// ----------

QString Mera_Luna::luna() const {
  return QString::fromStdString(d_->d_.luna());
}

bool Mera_Luna::hasLuna() const {
  return !d_->d_.luna().empty();
}

void Mera_Luna::setLuna(const QString& value) {
  d_->d_.set_luna(value.toStdString());
}

void Mera_Luna::clearLuna() {
  d_->d_.clear_luna();
}

// ----------

class Mera::__Data__ : public QSharedData {
 public:
  __pqpp__::Mera d_;
};

// ----------

Mera::Mera()
    : d_(new __Data__()) {}

Mera::Mera(const QByteArray    & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera::Mera(const QByteArray    & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera::Mera(const __pqpp__::Mera& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

Mera::Mera(const Mera&  value) = default;
Mera::Mera(      Mera&& value) = default;
Mera::~Mera()                  = default;

Mera& Mera::operator= (const Mera&  value) = default;
Mera& Mera::operator= (      Mera&& value) = default;

QByteArray
Mera::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::Mera& Mera::__pqpp__() const {
  return d_->d_;
}

// ----------

Mera_Luna Mera::luna() const {
  return Luna(d_->d_.luna());
}

bool Mera::hasLuna() const {
  return d_->d_.has_luna();
}

void Mera::setLuna(const Luna& value) {
  (*d_->d_.mutable_luna()) = value.__pqpp__();
}

void Mera::clearLuna() {
  d_->d_.clear_luna();
}

}  // namespace nm
}  // namespace li_void
