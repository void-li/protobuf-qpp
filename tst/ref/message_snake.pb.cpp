#include <QSharedData>
#include <QtGlobal>

#include "message_snake.pb.hpp"
#include "message_snake.pb.h"

// ----------

namespace li_void {
namespace ms {

class Mera_Luna_Moon_Hail::__Data__ : public QSharedData {
 public:
  __pqpp__::Mera_Luna_Moon_Hail d_;
};

// ----------

Mera_Luna_Moon_Hail::Mera_Luna_Moon_Hail()
    : d_(new __Data__()) {}

Mera_Luna_Moon_Hail::Mera_Luna_Moon_Hail(const QByteArray                   & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera_Luna_Moon_Hail::Mera_Luna_Moon_Hail(const QByteArray                   & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera_Luna_Moon_Hail::Mera_Luna_Moon_Hail(const __pqpp__::Mera_Luna_Moon_Hail& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

Mera_Luna_Moon_Hail::Mera_Luna_Moon_Hail(const Mera_Luna_Moon_Hail&  value) = default;
Mera_Luna_Moon_Hail::Mera_Luna_Moon_Hail(      Mera_Luna_Moon_Hail&& value) = default;
Mera_Luna_Moon_Hail::~Mera_Luna_Moon_Hail()                                 = default;

Mera_Luna_Moon_Hail& Mera_Luna_Moon_Hail::operator= (const Mera_Luna_Moon_Hail&  value) = default;
Mera_Luna_Moon_Hail& Mera_Luna_Moon_Hail::operator= (      Mera_Luna_Moon_Hail&& value) = default;

QByteArray
Mera_Luna_Moon_Hail::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::Mera_Luna_Moon_Hail& Mera_Luna_Moon_Hail::__pqpp__() const {
  return d_->d_;
}

// ----------

QString Mera_Luna_Moon_Hail::moon_hail() const {
  return QString::fromStdString(d_->d_.moon_hail());
}

bool Mera_Luna_Moon_Hail::hasMoon_hail() const {
  return !d_->d_.moon_hail().empty();
}

void Mera_Luna_Moon_Hail::setMoon_hail(const QString& value) {
  d_->d_.set_moon_hail(value.toStdString());
}

void Mera_Luna_Moon_Hail::clearMoon_hail() {
  d_->d_.clear_moon_hail();
}

// ----------

class Mera_Luna::__Data__ : public QSharedData {
 public:
  __pqpp__::Mera_Luna d_;
};

// ----------

Mera_Luna::Mera_Luna()
    : d_(new __Data__()) {}

Mera_Luna::Mera_Luna(const QByteArray         & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera_Luna::Mera_Luna(const QByteArray         & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera_Luna::Mera_Luna(const __pqpp__::Mera_Luna& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

Mera_Luna::Mera_Luna(const Mera_Luna&  value) = default;
Mera_Luna::Mera_Luna(      Mera_Luna&& value) = default;
Mera_Luna::~Mera_Luna()                       = default;

Mera_Luna& Mera_Luna::operator= (const Mera_Luna&  value) = default;
Mera_Luna& Mera_Luna::operator= (      Mera_Luna&& value) = default;

QByteArray
Mera_Luna::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::Mera_Luna& Mera_Luna::__pqpp__() const {
  return d_->d_;
}

// ----------

QString Mera_Luna::mera_luna() const {
  return QString::fromStdString(d_->d_.mera_luna());
}

bool Mera_Luna::hasMera_luna() const {
  return !d_->d_.mera_luna().empty();
}

void Mera_Luna::setMera_luna(const QString& value) {
  d_->d_.set_mera_luna(value.toStdString());
}

void Mera_Luna::clearMera_luna() {
  d_->d_.clear_mera_luna();
}

// ----------

Mera_Luna_Moon_Hail Mera_Luna::moon_hail() const {
  return Moon_Hail(d_->d_.moon_hail());
}

bool Mera_Luna::hasMoon_hail() const {
  return d_->d_.has_moon_hail();
}

void Mera_Luna::setMoon_hail(const Moon_Hail& value) {
  (*d_->d_.mutable_moon_hail()) = value.__pqpp__();
}

void Mera_Luna::clearMoon_hail() {
  d_->d_.clear_moon_hail();
}

}  // namespace ms
}  // namespace li_void
