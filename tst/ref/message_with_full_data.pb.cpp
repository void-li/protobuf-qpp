#include <QSharedData>
#include <QtGlobal>

#include "message_with_full_data.pb.hpp"
#include "message_with_full_data.pb.h"

// ----------

namespace li_void {
namespace mwfd {

class Mera::__Data__ : public QSharedData {
 public:
  __pqpp__::Mera d_;
};

// ----------

Mera::Mera()
    : d_(new __Data__()) {}

Mera::Mera(const QByteArray    & value,                   bool* is_ok)
    : d_(new __Data__()) {
  bool ok = d_->d_.ParseFromArray(value.data(), value.size());

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera::Mera(const QByteArray    & value, int pos, int len, bool* is_ok)
    : d_(new __Data__()) {
  Q_ASSERT(pos + len <= value.size());

  bool ok = d_->d_.ParseFromArray(value.data() + pos, len);

  if (is_ok) {
    *is_ok = ok;
  }
}

Mera::Mera(const __pqpp__::Mera& value)
    : d_(new __Data__()) {
  d_->d_ = value;
}

Mera::Mera(const Mera&  value) = default;
Mera::Mera(      Mera&& value) = default;
Mera::~Mera()                  = default;

Mera& Mera::operator= (const Mera&  value) = default;
Mera& Mera::operator= (      Mera&& value) = default;

QByteArray
Mera::serialize() const {
  int value_size = d_->d_.ByteSizeLong();

  QByteArray value;
  value.resize(value_size);

  d_->d_.SerializeToArray(value.data(), value.size());
  return value;
}

const __pqpp__::Mera& Mera::__pqpp__() const {
  return d_->d_;
}

// ----------

qint32 Mera::single_int32() const {
  return d_->d_.single_int32();
}

bool Mera::hasSingle_int32() const {
  return true;
}

void Mera::setSingle_int32(qint32 value) {
  d_->d_.set_single_int32(value);
}

void Mera::clearSingle_int32() {
  d_->d_.clear_single_int32();
}

// ----------

qint64 Mera::single_int64() const {
  return d_->d_.single_int64();
}

bool Mera::hasSingle_int64() const {
  return true;
}

void Mera::setSingle_int64(qint64 value) {
  d_->d_.set_single_int64(value);
}

void Mera::clearSingle_int64() {
  d_->d_.clear_single_int64();
}

// ----------

quint32 Mera::single_uint32() const {
  return d_->d_.single_uint32();
}

bool Mera::hasSingle_uint32() const {
  return true;
}

void Mera::setSingle_uint32(quint32 value) {
  d_->d_.set_single_uint32(value);
}

void Mera::clearSingle_uint32() {
  d_->d_.clear_single_uint32();
}

// ----------

quint64 Mera::single_uint64() const {
  return d_->d_.single_uint64();
}

bool Mera::hasSingle_uint64() const {
  return true;
}

void Mera::setSingle_uint64(quint64 value) {
  d_->d_.set_single_uint64(value);
}

void Mera::clearSingle_uint64() {
  d_->d_.clear_single_uint64();
}

// ----------

qint32 Mera::single_sint32() const {
  return d_->d_.single_sint32();
}

bool Mera::hasSingle_sint32() const {
  return true;
}

void Mera::setSingle_sint32(qint32 value) {
  d_->d_.set_single_sint32(value);
}

void Mera::clearSingle_sint32() {
  d_->d_.clear_single_sint32();
}

// ----------

qint64 Mera::single_sint64() const {
  return d_->d_.single_sint64();
}

bool Mera::hasSingle_sint64() const {
  return true;
}

void Mera::setSingle_sint64(qint64 value) {
  d_->d_.set_single_sint64(value);
}

void Mera::clearSingle_sint64() {
  d_->d_.clear_single_sint64();
}

// ----------

bool Mera::single_bool() const {
  return d_->d_.single_bool();
}

bool Mera::hasSingle_bool() const {
  return true;
}

void Mera::setSingle_bool(bool value) {
  d_->d_.set_single_bool(value);
}

void Mera::clearSingle_bool() {
  d_->d_.clear_single_bool();
}

// ----------

quint32 Mera::single_fixed32() const {
  return d_->d_.single_fixed32();
}

bool Mera::hasSingle_fixed32() const {
  return true;
}

void Mera::setSingle_fixed32(quint32 value) {
  d_->d_.set_single_fixed32(value);
}

void Mera::clearSingle_fixed32() {
  d_->d_.clear_single_fixed32();
}

// ----------

qint32 Mera::single_sfixed32() const {
  return d_->d_.single_sfixed32();
}

bool Mera::hasSingle_sfixed32() const {
  return true;
}

void Mera::setSingle_sfixed32(qint32 value) {
  d_->d_.set_single_sfixed32(value);
}

void Mera::clearSingle_sfixed32() {
  d_->d_.clear_single_sfixed32();
}

// ----------

float Mera::single_float() const {
  return d_->d_.single_float();
}

bool Mera::hasSingle_float() const {
  return true;
}

void Mera::setSingle_float(float value) {
  d_->d_.set_single_float(value);
}

void Mera::clearSingle_float() {
  d_->d_.clear_single_float();
}

// ----------

quint64 Mera::single_fixed64() const {
  return d_->d_.single_fixed64();
}

bool Mera::hasSingle_fixed64() const {
  return true;
}

void Mera::setSingle_fixed64(quint64 value) {
  d_->d_.set_single_fixed64(value);
}

void Mera::clearSingle_fixed64() {
  d_->d_.clear_single_fixed64();
}

// ----------

qint64 Mera::single_sfixed64() const {
  return d_->d_.single_sfixed64();
}

bool Mera::hasSingle_sfixed64() const {
  return true;
}

void Mera::setSingle_sfixed64(qint64 value) {
  d_->d_.set_single_sfixed64(value);
}

void Mera::clearSingle_sfixed64() {
  d_->d_.clear_single_sfixed64();
}

// ----------

double Mera::single_double() const {
  return d_->d_.single_double();
}

bool Mera::hasSingle_double() const {
  return true;
}

void Mera::setSingle_double(double value) {
  d_->d_.set_single_double(value);
}

void Mera::clearSingle_double() {
  d_->d_.clear_single_double();
}

// ----------

QString Mera::single_string() const {
  return QString::fromStdString(d_->d_.single_string());
}

bool Mera::hasSingle_string() const {
  return !d_->d_.single_string().empty();
}

void Mera::setSingle_string(const QString& value) {
  d_->d_.set_single_string(value.toStdString());
}

void Mera::clearSingle_string() {
  d_->d_.clear_single_string();
}

// ----------

QByteArray Mera::single_bytes() const {
  return QByteArray::fromStdString(d_->d_.single_bytes());
}

bool Mera::hasSingle_bytes() const {
  return !d_->d_.single_bytes().empty();
}

void Mera::setSingle_bytes(const QByteArray& value) {
  d_->d_.set_single_bytes(value.toStdString());
}

void Mera::clearSingle_bytes() {
  d_->d_.clear_single_bytes();
}

}  // namespace mwfd
}  // namespace li_void
